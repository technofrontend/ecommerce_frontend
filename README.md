# E-commerce App
---
## Run Locally
### 1. Install Dependencies
```sh
$ npm install
```

### 2. Run development server
```sh 
$ npm start
```

---