import React, { Component } from "react";
import "./App.css";
import Authenticate from "./components/Forms/Authenticate/Authenticate";

import AppRoute from "./Routes/AppRoute";
// import NotFoundPage from "./components/Shared/404Error/NotFoundPage";

class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      user: false,
    };
  }

  checkUser = async () => {
    let currentUser = await Authenticate.getLoggedInUser();
    if (currentUser) {
      this.setState({
        user: true,
      });
    }
  };

  componentDidMount() {
    this.checkUser();
  }
  render() {
    let { user } = this.state;
    return <AppRoute user={user} />;
  }
}

export default App;
