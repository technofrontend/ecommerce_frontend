import axios from "axios";
import { CategoryData } from "./Categories";
import { BannerData } from "./Banners";

import Login from "./loginData.json";
import Register from "./registerData.json";
import Cookies from "universal-cookie";

const HOST = "https://protected-basin-07111.herokuapp.com/";
let data = [];

const getCategory = () => {
  let catNames = axios({
    method: "GET",
    url: `${HOST}product/getcategories`,
    headers: {
      "Content-Type": "application/json",
    },
  })
    .then((res) => {
      data = res.data;
      return data;
    })
    .catch((err) => {
      return err.response.data;
    });
  return catNames;
};

export const GetCategoryBannerData = async () => {
  data = await getCategory().then((res) => {
    return res;
  });
  const Category = data.map((cat) => {
    const category = CategoryData[cat];
    if (!category) {
      return false;
    }
    return category;
  });

  let Banner = data.map((item) => {
    const banner = BannerData[item];
    if (!banner) {
      return false;
    }
    return banner;
  });

  return [Category, Banner];
};

export const getCategoryWiseProduct = async (catName) => {
  data = await getCategory().then((res) => {
    return res;
  });

  const Banner = BannerData[catName];

  let productList = await axios({
    method: "GET",
    url: `${HOST}product/viewcategory`,
    headers: {
      "Content-Type": "application/json",
    },
    params: {
      category: catName,
    },
  })
    .then((res) => {
      return res;
    })
    .catch((err) => {
      return err;
    });

  return [data, productList.data, Banner];
};

export const getSingleProduct = async (id) => {
  data = await getCategory().then((res) => {
    return res;
  });

  let product = await axios({
    method: "GET",
    url: `${HOST}product/view`,
    headers: {
      "Content-Type": "application/json",
    },
    params: {
      product: id,
    },
  })
    .then((res) => {
      return res;
    })
    .catch((err) => {
      return err;
    });

  return [product.data, data];
};

export const LoginForm = () => {
  return Login;
};

export const RegisterForm = () => {
  return Register;
};

export const AddtoWishlist = (pid, e) => {
  e.preventDefault();
  const cookie = new Cookies();
  const jwtToken = cookie.get("uuid");
  try {
    fetch("https://protected-basin-07111.herokuapp.com/wishlist/update", {
      method: "post",
      headers: {
        "Content-type": "application/json;charset=UTF-8",
        Authorization: "Bearer " + jwtToken,
      },
      body: JSON.stringify({
        productid: pid,
        delete: "false",
      }),
    });
  } catch (e) {
    console.log(e);
  }
};

export const AddtoCart = (pid, quant, e) => {
  e.preventDefault();
  const cookie = new Cookies();
  const jwtToken = cookie.get("uuid");
  try {
    fetch("https://protected-basin-07111.herokuapp.com/user/cart/update-cart", {
      method: "post",
      headers: {
        "Content-type": "application/json;charset=UTF-8",
        Authorization: "Bearer " + jwtToken,
      },
      body: JSON.stringify({
        product: {
          quantity: quant,
          pid: pid,
        },
      }),
    });
  } catch (e) {
    console.log(e);
  }
};
