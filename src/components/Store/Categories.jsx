export const CategoryData = {
  "GROCERY": {
    "id": 1,
    "image":
      "https://images.unsplash.com/photo-1542838132-92c53300491e?ixid=MXwxMjA3fDB8MHxzZWFyY2h8MXx8Z3JvY2VyeXxlbnwwfHwwfA%3D%3D&ixlib=rb-1.2.1&auto=format&fit=crop&w=1296&q=60",
    "path": "grocery/pl",
    "alt": "Grocery",
    "label": "Grocery",
    "text": "Try out our Farm Fresh products",
  },
  "FASHION": {
    "id": 2,

    "image":
      "https://images.unsplash.com/photo-1523381210434-271e8be1f52b?ixid=MXwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHw%3D&ixlib=rb-1.2.1&auto=format&fit=crop&w=1350&q=80",
    "path": "fashion/pl",
    "alt": "Fashion",
    "label": "Fashion",
    "text": "Always be Upto date with the Trends",
  },
  "ELECTRONICS": {
    "id": 3,

    "image":
      "https://images.unsplash.com/photo-1498049794561-7780e7231661?ixid=MXwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHw%3D&ixlib=rb-1.2.1&auto=format&fit=crop&w=1350&q=80",
    "path": "electronics/pl",
    "alt": "Electronics",
    "label": "Electronics",
    "text": "Content for the perfect tech geeks",
  },
  "BOOKS": {
    "id": 4,

    "image":
      "https://images.unsplash.com/photo-1481627834876-b7833e8f5570?ixid=MXwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHw%3D&ixlib=rb-1.2.1&auto=format&fit=crop&w=1141&q=80",
    "path": "books/pl",
    "alt": "Books",
    "label": "Books",
    "text": "Dive into our vast collection of books",
  },
  "SPORTS": {
    "id": 5,

    "image":
      "https://images.unsplash.com/photo-1461896836934-ffe607ba8211?ixid=MXwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHw%3D&ixlib=rb-1.2.1&auto=format&fit=crop&w=1350&q=80",
    "path": "sports/pl",
    "alt": "Sports",
    "label": "Sports",
    "text": "Eat, Sleep , Workout , Repeat!!!",
  },
  "OTHERS": {
    "id": 6,

    "image":
      "https://images.unsplash.com/photo-1461896836934-ffe607ba8211?ixid=MXwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHw%3D&ixlib=rb-1.2.1&auto=format&fit=crop&w=1350&q=80",
    "path": "others/pl",
    "alt": "Others",
    "label": "Others",
    "text": "Browse our miscellaneous Products",
  },
};
