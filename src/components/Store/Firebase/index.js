import firebase from "firebase/app";
import "firebase/storage";

//  Firebase configuration
const firebaseConfig = {
  apiKey: "AIzaSyBbhqLO7WAbOZr7NdBxbVAxHz6aRvrSCiY",
  authDomain: "ecomapp1-a9fca.firebaseapp.com",
  databaseURL: "https://ecomapp1-a9fca-default-rtdb.firebaseio.com/",
  projectId: "ecomapp1-a9fca",
  storageBucket: "gs://ecomapp1-a9fca.appspot.com",
  messagingSenderId: "43597918523958",
  appId: "234598789798798fg3-034"
};

// Initialize Firebase

firebase.initializeApp(firebaseConfig);

const storage = firebase.storage();

export { firebase, storage as default };
