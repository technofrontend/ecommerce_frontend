import React, { Component } from "react";
import "./Ratings.css";

export default class RatingStars extends Component {
  rating = () => {
    const starTotal = 5;
    const ratings = this.props.rating;
    const starValue = (ratings / starTotal) * 100;
    const starPercentage = `${(starValue / 10) * 10}%`;

    document.getElementById(
      `star${this.props.id}`
    ).style.width = starPercentage;
  };

  componentDidMount() {
    this.rating();
  }
  render() {
    let { id } = this.props;
    return (
      <div className="stars-outer">
        <i className="far fa-star" />
        <div className="stars-inner" id={`star${id}`}>
          <i className="fa fa-star" />
        </div>
      </div>
    );
  }
}
