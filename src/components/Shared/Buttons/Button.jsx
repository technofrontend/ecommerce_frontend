import React, { Component } from "react";
import "./Buttons.css";

export default class Button extends Component {
  render() {
    let { text, type, Click, className, itag, divClass, disabled } = this.props;
    return (
      <button
        className={className}
        type={type}
        onClick={Click}
        disabled={disabled}
      >
        <div className={divClass}>
          <i className={itag} />
        </div>
        {text}
      </button>
    );
  }
}
