import React, { Component } from "react";
import { Link } from "react-router-dom";

import "./FooterCol.css";

export default class FooterCol extends Component {
  render() {
    let { name, data } = this.props;
    return (
      <div className="footer_column">
        <h2>{name}</h2>
        <ul>
          {data.map((item, index) => {
            return (
              <li key={index}>
                <Link className={item.cname} to={item.path}>
                  {item.title} <i className={item.logo}></i>
                </Link>
              </li>
            );
          })}
        </ul>
      </div>
    );
  }
}
