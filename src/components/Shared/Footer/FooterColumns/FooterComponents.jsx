export const AboutItems = [
  {
    title: "Story",
    path: "/Story",
    cname: "footer_link",
  },
  {
    title: "Clients",
    path: "/Clients",
    cname: "footer_link",
  },
  {
    title: "Testimonials",
    path: "/Testimonials",
    cname: "footer_link",
  },
  {
    title: "Contact Us",
    path: "/Contact Us",
    cname: "footer_link",
  },
];
export const Services = [
  {
    title: "Design",
    path: "/Design",
    cname: "footer_link",
  },
  {
    title: "Development",
    path: "/Developement",
    cname: "footer_link",
  },
  {
    title: "Marketing",
    path: "/Marketing",
    cname: "footer_link",
  },
];
export const Socials = [
  {
    title: "Facebook",
    path: "/facebookpath",
    cname: "footer_link",
    logo: "fab fa-facebook-square",
  },
  {
    title: "Instagram",
    path: "/instapath",
    cname: "footer_link",
    logo: "fab fa-instagram",
  },
  {
    title: "Twitter",
    path: "/twitterpath",
    cname: "footer_link",
    logo: "fab fa-twitter",
  },
];
