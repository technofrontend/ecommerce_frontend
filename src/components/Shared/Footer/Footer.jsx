import React, { Component } from "react";

import {
  AboutItems,
  Services,
  Socials,
} from "./FooterColumns/FooterComponents";
import "./Footer.css";
import FooterCol from "./FooterColumns/FooterCol";

export default class Footer extends Component {
  render() {
    return (
      <div className="footer">
        <div className="footer_container">
          <div className="footer_row">
            <FooterCol data={AboutItems} name="About Us" />
            <FooterCol name="Services" data={Services} />
            <FooterCol name="Socials" data={Socials} />
          </div>
          <hr />
          <div className="footer_row">
            <p>
              &copy;{new Date().getFullYear()} Company Name Inc | ALL Rights
              Reserved | Terms of Service | Privacy
            </p>
          </div>
        </div>
      </div>
    );
  }
}
