import React, { Component } from "react";
import FavoriteBorderIcon from "@material-ui/icons/FavoriteBorder";

import "./WishlistIcon.css";

export default class WishlistIcon extends Component {
  render() {
    return (
      <div className="navbar_wishlist">
        <FavoriteBorderIcon
          className="navbar_wishlistIcon"
          fontSize={"small"}
        />
        <span className="navbar_wishlistText">Wishlist</span>
      </div>
    );
  }
}
