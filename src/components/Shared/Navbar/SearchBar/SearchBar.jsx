import React, { Component } from "react";
import SearchIcon from "@material-ui/icons/Search";

import "./SearchBar.css";

export default class SearchBar extends Component {
  render() {
    return (
      <div className="navbar_search">
        <input type="text" placeholder="Search Products" />
        <SearchIcon className="navbar_searchIcon" />
      </div>
    );
  }
}
