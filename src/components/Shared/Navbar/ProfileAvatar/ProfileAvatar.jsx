import React, { Component } from "react";
import { Link } from "react-router-dom";
import { DropdownItems } from "./DropdownItems/DropdownItems";

import "./ProfileAvatar.css";

export default class ProfileAvatar extends Component {
  constructor(props) {
    super(props);
    this.state = {
      click: false,
    };
  }

  handleClick = () => {
    let clicked = this.state.click;

    this.setState({ click: !clicked });
  };

  render() {
    let { click } = this.state;
    return (
      <>
        <ul
          onClick={this.handleClick}
          className={click ? "dropdown_menu clicked" : "dropdown_menu"}
        >
          {DropdownItems.map((item, index) => {
            return (
              <li key={index}>
                <Link
                  className={item.cName}
                  to={item.path}
                  onClick={() => {
                    this.setState({ click: false });
                  }}
                >
                  {item.title}
                </Link>
              </li>
            );
          })}
        </ul>
      </>
    );
  }
}
