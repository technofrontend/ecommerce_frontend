export const DropdownItems = [
  {
    title: "My orders",
    path: "/orders",
    cName: "dropdown_link",
  },
  {
    title: "Edit Profile",
    path: "/profile",
    cName: "dropdown_link",
  },
  {
    title: "Settings",
    path: "/settings",
    cName: "dropdown_link",
  },
];
