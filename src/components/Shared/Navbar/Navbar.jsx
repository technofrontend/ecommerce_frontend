import React, { Component } from "react";
import { Link, withRouter } from "react-router-dom";

import Logo from "./Logo/Logo";
import SearchBar from "./SearchBar/SearchBar";
import WishlistIcon from "./WishlistIcon/WishlistIcon";
import CartBasket from "./CartBasket/CartBasket";
import ProfileAvatar from "./ProfileAvatar/ProfileAvatar";
import "./Navbar.css";
import Authenticate from "../../Forms/Authenticate/Authenticate";

class Navbar extends Component {
  constructor(props) {
    super(props);
    this.state = {
      click: false,
      dropdown: false,
      isLoggedIn: false,
      user: {},
    };
  }

  handleClick = () => {
    let changeClick = this.state.click;

    this.setState({ click: !changeClick });
  };

  handleCloseMenu = () => {
    this.setState({ click: false });
  };

  onMouseEnter = () => {
    if (window.innerWidth < 960) {
      this.setState({ dropdown: false });
    } else {
      this.setState({ dropdown: true });
    }
  };

  onMouseLeave = () => {
    if (window.innerWidth < 960) {
      this.setState({ dropdown: false });
    } else {
      this.setState({ dropdown: false });
    }
  };

  handleLogout = () => {
    console.log("Logout");
    Authenticate.handleUserLogout();
    this.setState({ isLoggedIn: false });
    this.props.history.push("/");
  };

  static getDerivedStateFromProps(state) {
    const userData = localStorage.getItem("user");
    const user = JSON.parse(userData);

    if (user) {
      if (state.user !== userData) {
        return {
          user: user,
          isLoggedIn: true,
        };
      }
    }

    return null;
  }

  render() {
    let { click, dropdown, isLoggedIn, user } = this.state;

    return (
      <nav className="navbar">
        <Link to="/" className="navbar_logo">
          <Logo />
        </Link>
        <SearchBar />
        <div className="menu_icon" onClick={this.handleClick}>
          <i className={click ? "fas fa-times" : "fas fa-bars"}></i>
        </div>
        <ul className={click ? "navbar_menu active" : "navbar_menu"}>
          <li className="navbar_item">
            <Link
              to="/Wishlist"
              className="navbar_links"
              onClick={this.handleCloseMenu}
            >
              <WishlistIcon />
            </Link>
          </li>
          <li className="navbar_item">
            <Link
              to="/myCart"
              className="navbar_links"
              onClick={this.handleCloseMenu}
            >
              <CartBasket />
            </Link>
          </li>

          {isLoggedIn === true ? (
            <li
              className="navbar_item"
              onMouseEnter={this.onMouseEnter}
              onMouseLeave={this.onMouseLeave}
            >
              <Link
                to=""
                className="navbar_links"
                onClick={this.handleCloseMenu}
              >
                Hi, {user ? user.username : " "}
                <i className="fas fa-caret-down" />
              </Link>
              {dropdown && <ProfileAvatar />}
            </li>
          ) : (
            <li className="navbar_item">
              <Link
                to="/login"
                className="navbar_links"
                onClick={this.handleCloseMenu}
              >
                Sign In
              </Link>
            </li>
          )}

          {isLoggedIn === true && (
            <li className="navbar_item">
              <Link to="" className="navbar_links" onClick={this.handleLogout}>
                LOGOUT
              </Link>
            </li>
          )}
        </ul>
      </nav>
    );
  }
}

export default withRouter(Navbar);
