import React, { Component } from "react";
import ShoppingCartIcon from "@material-ui/icons/ShoppingCart";
import "./CartBasket.css";

export default class CartBasket extends Component {
  render() {
    return (
      <div className="navbar_cart">
        <ShoppingCartIcon className="navbar_cartIcon" fontSize={"small"} />
        <span className="navbar_cartText">MyCart</span>
      </div>
    );
  }
}
