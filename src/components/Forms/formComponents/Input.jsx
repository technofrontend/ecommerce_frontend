import React, { Component } from "react";
import "./Input.css";

export default class Input extends Component {
  render() {
    let { data } = this.props;
    let field = (
      <div className="input_container">
        <label className="input_label" htmlFor={data.id}>
          {data.label}{" "}
        </label>
        <input
          className="input_inputBox"
          name={data.name}
          type={data.type}
          placeholder={data.value}
          value={this.props.value}
          required={data.required}
          onChange={(e) => this.props.action(e, data.name)}
          id={data.id}
        />
      </div>
    );

    if (data.type === "radio") {
      let field = data.options.map((option, i) => {
        return (
          <div className="input__radio" key={i}>
            <label className="input__radioLabel" htmlFor={option.name}>
              {option.label}
            </label>
            <input
              className="input__radioButton"
              type={data.type}
              name={option.name}
              value={this.props.value}
              onClick={(e) => this.props.action(e, option.value)}
            />
          </div>
        );
      });

      return (
        <div className="input__radioContainer">
          <label id="input__label" htmlFor="">
            {data.label}
          </label>
          {field}
        </div>
      );
    }

    return <div className="input">{field}</div>;
  }
}
