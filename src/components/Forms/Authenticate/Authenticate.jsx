import Cookies from "universal-cookie";
import jwt from "jwt-decode";
import axios from "axios";

const Host = "https://protected-basin-07111.herokuapp.com/";

let user = {
  role: "",
  username: "",
};

class Authenticate {
  cookies = new Cookies();

  authenticateUser(username, password) {
    return axios({
      method: "POST",
      url: `${Host}users/login`,
      headers: {
        crossDomain: true,
        "Content-Type": "application/json",
      },
      data: {
        email_phone: username,
        password: password,
      },
    })
      .then((res) => {
        console.log(res);
        user.role = res.data.userinfo.role;
        user.username = res.data.userinfo.username;
        let token = res.data.userinfo.token;
        let tokenData = jwt(token);

        const expiryDate = new Date(tokenData.exp * 1000);
        if (token) {
          this.cookies.set("uuid", token, {
            path: "/",
            expires: expiryDate,
          });
        }

        localStorage.setItem("user", JSON.stringify(user));

        return res;
      })
      .catch((err) => {
        return err.response.data;
      });
  }

  handleUserLogout() {
    this.cookies.remove("uuid");
    localStorage.removeItem("user");
    return true;
  }

  getLoggedInUser() {
    let cookie = this.cookies.get("uuid");
    if (cookie) {
      return jwt(cookie);
    } else return undefined;
  }

  async registerUser(user) {
    console.log(user);
    return axios({
      method: "POST",
      url: `${Host}users/save`,
      headers: {
        crossDomain: true,
        "Content-Type": "application/json",
      },
      data: {
        user,
      },
    })
      .then((res) => {
        console.log(res);
      })
      .catch((err) => {
        console.log(err);
      });
  }
}

export default new Authenticate();
// {
//     "email": "shubhambhardwaj@gmail.com",
//     "password": "123456",
//     "firstname": "shubham",
//     "lastname": "bhardwaj",
//     "age": "24",
//     "phoneno": "9654741157",
//     "role": "Seller",
//     "address": {
//         "houseno": "shakti vihar",
//         "city": "Delhi",
//         "country": "india",
//         "pincode": "110034"
//     }
// }

// {
//     "id": 15,
//     "firstname": "shubham",
//     "lastname": "bhardwaj",
//     "phoneno": "9654742157",
//     "email": "shubhambhardwaj142@gmail.com",
//     "address": {
//         "address": "shakti vihar",
//         "city": "Delhi",
//         "country": "india",
//         "pincode": "110034"
//     },
//     "role": "BUYER"
// }
