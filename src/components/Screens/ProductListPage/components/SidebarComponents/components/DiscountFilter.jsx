import React, { Component } from "react";
import { Link } from "react-router-dom";
import "./Filters.css";

export default class DiscountFilter extends Component {
  render() {
    return (
      <div className="filter__container">
        <h4> Discount</h4>
        <div className="filter__linksContainer">
          <Link to="" className="filter__links">
            10% Off or more
          </Link>
          <Link to="" className="filter__links">
            25% Off or more
          </Link>
          <Link to="" className="filter__links">
            35% Off or more
          </Link>
          <Link to="" className="filter__links">
            50% Off or more
          </Link>
        </div>
      </div>
    );
  }
}
