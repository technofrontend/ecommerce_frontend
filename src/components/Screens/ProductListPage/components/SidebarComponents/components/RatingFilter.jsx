import React, { Component } from "react";
import { Link } from "react-router-dom";
import _ from "lodash";
import "./Filters.css";

export default class RatingFilter extends Component {
  render() {
    let count = 4;
    return (
      <div className="filter__container">
        <h4> Avg. Customer Review</h4>
        <div className="ratingFilter__starsContainer">
          {_.times(count, (i) => (
            <div key={i} className="ratingFilter__stars">
              <Link to="" className="rating__container__link">
                <div className="ratingStars-outer">
                  <i className="far fa-star" />
                  <div className={`ratingStars-inner star${i}`}>
                    <i className="fa fa-star" />
                  </div>
                </div>
              </Link>
              <p> & Up</p>
            </div>
          ))}
        </div>
      </div>
    );
  }
}
