import React, { Component } from "react";
import { Link } from "react-router-dom";
import "./Filters.css";

export default class PriceFilter extends Component {
  render() {
    return (
      <div className="filter__container">
        <h4>Price</h4>
        <div className="filter__linksContainer">
          <Link to="" className="filter__links">
            under ₹500
          </Link>
          <Link to="" className="filter__links">
            ₹500 - ₹1000
          </Link>
          <Link to="" className="filter__links">
            ₹1000 - ₹5000
          </Link>
          <Link to="" className="filter__links">
            ₹5000 - ₹10000
          </Link>
          <Link to="" className="filter__links">
            over ₹10000
          </Link>
        </div>
      </div>
    );
  }
}
