import React, { Component } from "react";
// import CategoryFilter from "./components/CategoryFilter";
import DiscountFilter from "./components/DiscountFilter";
import PriceFilter from "./components/PriceFilter";
import RatingFilter from "./components/RatingFilter";

export default class SideBar extends Component {
  render() {
    return (
      <div className="pl__body__main__sidebar">
        <PriceFilter />
        <RatingFilter />
        <DiscountFilter />
        {/* <CategoryFilter /> */}
      </div>
    );
  }
}
