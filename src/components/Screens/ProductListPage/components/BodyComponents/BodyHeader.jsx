import React, { Component } from "react";
import { Link, withRouter } from "react-router-dom";

class BodyHeader extends Component {
  // click = (path) => {
  //   this.props.history.push(path);
  //   // window.location.reload();
  // };
  render() {
    let { data } = this.props;

    return (
      <div className="pl__body__header">
        {data &&
          data.map((item, index) => {
            return (
              <Link
                to={`/${item.toLowerCase()}/pl`}
                // onClick={() => {
                //   this.click(`/${item.toLowerCase()}/pl`);
                // }}
                className="pl__body__header__links"
                key={index}
              >
                {item}
              </Link>
            );
          })}
      </div>
    );
  }
}

export default withRouter(BodyHeader);
