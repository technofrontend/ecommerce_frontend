import React, { Component } from "react";
import "./BodyBanner.css";

export default class BodyBanner extends Component {
  render() {
    let { data } = this.props;
    return (
      <div className="bodyBanner__main">
        <div className="bodyBanner__textContainer">
          <p className="bodyBanner__text">{data.text}</p>
        </div>
        <img src={data.image_Pl} alt={data.alt} className="bodyBanner__img" />
      </div>
    );
  }
}
