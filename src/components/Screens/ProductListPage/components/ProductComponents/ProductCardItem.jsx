import React, { Component } from "react";
import { Link } from "react-router-dom";
import Button from "../../../../Shared/Buttons/Button";
import RatingStars from "../../../../Shared/Ratings/RatingStars";
import { AddtoWishlist } from "../../../../Store/fetchCalls";
export default class ProductCardItem extends Component {
  render() {
    let { src, name, actualP, listedP, id, rating } = this.props;
    let r = Math.random().toString(36).substring(7);

    return (
      <div className="productCard__container">
        <div className="productCard__item">
          <Link
            to={{ pathname: `/z${id}/${name}`, state: r }}
            className="productCard__links"
          >
            <img className="productCard__item__img" src={src} alt={name} />
          </Link>
          <div className="productCard__item__info">
            <hr />
            <div className="productCard__item__textRating">
              <h5 className="productCard__item__text">{name}</h5>
              <div className="rating__container">
                <Link to="" className="rating__container__link">
                  <RatingStars id={id} rating={rating} />
                  <h5>{rating}/5</h5>
                </Link>
              </div>
            </div>

            <div className="productCard__item__price">
              <span className="productCard__item__listedP">₹{listedP}</span>
              <span className="productCard__item__actualP">₹{actualP}</span>
            </div>
          </div>
          <div className="productCard__item__buttonContainer">
            {/* <Button text={"Buy"} className={"productCard__item__button"} /> */}
            <Button
              Click={(e) => {
                AddtoWishlist(id, e);
              }}
              text={"Add to WishList"}
              className={"productCard__item__button"}
            />
          </div>
        </div>
      </div>
    );
  }
}
