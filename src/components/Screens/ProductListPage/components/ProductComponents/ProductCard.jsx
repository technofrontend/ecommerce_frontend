import React, { Component } from "react";
import ProductCardItem from "./ProductCardItem";

import "./ProductCard.css";

export default class ProductCard extends Component {
  render() {
    let { data } = this.props;

    return (
      <div className="product__items">
        {data &&
          data.map((item, index) => {
            return (
              <ProductCardItem
                key={index}
                src={item.info.imageurl}
                name={item.name}
                listedP={item.listedprice}
                actualP={item.actualprice}
                rating={item.avgrating}
                id={item.pid}
              />
            );
          })}
      </div>
    );
  }
}
