import React, { Component } from "react";
import "./ProductList.css";
import ProductCard from "./components/ProductComponents/ProductCard";
import BodyHeader from "./components//BodyComponents/BodyHeader";
import BodyBanner from "./components/BodyComponents/BodyBanner";
import { getCategoryWiseProduct } from "../../Store/fetchCalls";
import SideBar from "./components/SidebarComponents/SideBar";

export default class ProductList extends Component {
  constructor(props) {
    super(props);

    this.state = {
      data: [],
      names: [],
      bannerData: [],
      fetching: true,
    };
  }

  fetchCall = () => {
    let categoryName = this.props.data.params.id.toUpperCase();
    getCategoryWiseProduct(categoryName).then((res) => {
      this.setState({
        names: res[0],
        data: res[1],
        bannerData: res[2],
        fetching: false,
      });
    });
  };

  componentDidUpdate(prevProps) {
    let currentPath = this.props.location.pathname;
    let prevPath = prevProps.location.pathname;

    if (currentPath !== prevPath) {
      this.fetchCall();
    }
  }

  componentDidMount() {
    console.log(this.props);
    this.fetchCall();
  }

  render() {
    let { names, data, bannerData, fetching } = this.state;
    if (fetching === true) {
      return <h1>LOADING....</h1>;
    }

    return (
      <div className="pl__body">
        <BodyHeader data={names} />
        <hr />
        <div className="pl__body__main">
          <SideBar />
          <hr className="body__hr" />
          <div className="pl__body__main__content">
            <BodyBanner data={bannerData} />
            <div className="pl__body__main__items">
              <p>Wide Range of Products</p>
              <div className="pl__body__main__grid">
                <ProductCard data={data} />
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}
