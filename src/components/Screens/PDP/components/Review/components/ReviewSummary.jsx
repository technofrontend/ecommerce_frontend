import React, { Component } from "react";
import RatingStars from "../../../../../Shared/Ratings/RatingStars";
import "./ReviewDesign.css";

export default class ReviewSummary extends Component {
  render() {
    let { data } = this.props;
    return (
      <div className="reviewSummary__main">
        <h2>Customer Reviews</h2>
        <div className="rating__stars">
          <RatingStars
            className="stars"
            id={data.id + 1}
            rating={data.rating}
          />
          <h4>{data.rating} out of 5</h4>
        </div>
        <h4 className="reviewSummary__ratings">
          {data.reviews.length} Ratings
        </h4>
      </div>
    );
  }
}
