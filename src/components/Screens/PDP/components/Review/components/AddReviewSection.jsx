import React, { Component } from "react";

export default class AddReviewSection extends Component {
  render() {
    return (
      <div className="addReview">
        <h2>Review This Product</h2>
        <h4>Share your thoughts with other customers</h4>
        <button className="addReview__button">Write Product Review</button>
      </div>
    );
  }
}
