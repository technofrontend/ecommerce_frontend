import React, { Component } from "react";
import ReviewCard from "./ReviewCard";

export default class DisplayReview extends Component {
  render() {
    let { data } = this.props;
    return (
      <div className="displayReview">
        <h2>Check out our top Reviews</h2>
        <div className="displayReview__main">
          {data &&
            data.reviews.map((review, index) => {
              return <ReviewCard key={index} data={review} />;
            })}
        </div>
      </div>
    );
  }
}
