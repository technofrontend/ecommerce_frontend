import React, { Component } from "react";
import RatingStars from "../../../../../Shared/Ratings/RatingStars";

export default class ReviewCard extends Component {
  render() {
    let { data } = this.props;
    console.log(data);
    let img = data.img
      ? data.img
      : "https://www.w3schools.com/howto/img_avatar2.png";
    return (
      <div className="reviewCard">
        <div className="reviewCard__head">
          <div className="reviewCard__user">
            <img src={img} alt="Avatar" className="reviewCard__avatar" />
            <h5>{data.username ? data.username : "Customer"}</h5>
          </div>
          <div className="reviewCard__rating">
            <RatingStars id={data.reviewid + "review"} rating={data.rating} />
            <p>{data.title ? data.title : "Review Title"}</p>
          </div>
        </div>
        <div className="reviewCard__body">
          <p>{data.comment}</p>
          <div className="reviewCard__response">
            <div className="reviewCard__like">
              <p>{data.likes ? data.likes : 0}</p>
              <i className="far fa-thumbs-up" />
            </div>

            <div className="reviewCard__dislike">
              <p>{data.dislikes ? data.dislikes : 0}</p>

              <i className="far fa-thumbs-down" />
            </div>
          </div>
        </div>
      </div>
    );
  }
}
