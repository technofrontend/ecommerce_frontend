import React, { Component } from "react";
import AddReviewSection from "./components/AddReviewSection";
import DisplayReview from "./components/DisplayReview";
import ReviewSummary from "./components/ReviewSummary";
import "./ReviewSection.css";
export default class ReviewSection extends Component {
  render() {
    let { data, user } = this.props;

    let summary = {
      rating: data.avgrating,
      reviews: data.reviewlist,
      id: data.pid,
    };

    return (
      <div className="reviewSection__main">
        <div className="reviewSection__left">
          <ReviewSummary data={summary} />
          {user === true ? <AddReviewSection /> : ""}
        </div>
        <div className="reviewSection__right">
          <DisplayReview data={summary} />
        </div>
      </div>
    );
  }
}
