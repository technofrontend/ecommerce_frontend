import React, { Component } from "react";

export default class DetailSection extends Component {
  render() {
    let { info } = this.props;
    return (
      <div className="productDesc_details">
        <div className="productDesc__availability">
          {info.unitsleft >= 10 ? (
            <h3 className="productDesc__available">In Stock</h3>
          ) : info.unitsleft < 10 && info.unitsleft > 0 ? (
            <h3 className="productDesc__limited">Hurry Up</h3>
          ) : (
            <h3 className="productDesc__unavailable">Out of Stock</h3>
          )}
        </div>
        <div className="productDesc__description">
          <p>{info.description}</p>
        </div>
      </div>
    );
  }
}
