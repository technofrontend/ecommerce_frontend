import React, { Component } from "react";

export default class PriceSection extends Component {
  render() {
    let { actualP, listedP } = this.props;
    let savings = actualP - listedP;
    let savingPercent = Math.round((savings / actualP) * 100);

    return (
      <div className="productDesc__price">
        <div className="productDesc__mrp">
          <h4>M.R.P.:</h4>
          <h5>₹ {actualP}.00</h5>
        </div>
        <div className="productDesc__listed">
          <h4>Price:</h4>
          <h5>₹ {listedP}.00</h5>
        </div>
        <div className="productDesc__savings">
          <h4>You Save:</h4>
          <div className="productDesc__savingsInner">
            <h5>
              ₹ {savings}.00({savingPercent}%)
            </h5>
            <p>Inclusive of all taxes</p>
          </div>
        </div>
        <div className="productDesc__pricePolicies">
          <div className="paymentMethod">
            <div className="method__icon">
              <i className="fas fa-wallet" />
              <p>Pay with Wallet</p>
            </div>

            <div className="method__details">
              <h5>What is Pay with Wallet?</h5>
              <h6>
                Pay with Wallet payment method includes payment with your wallet
                amount.
              </h6>
            </div>
          </div>

          <div className="deliveryMethod">
            <div className="method__icon">
              <i className="fas fa-truck" />
              <p>Fast Delivery</p>
            </div>
            <div className="method__details">
              <h5>What is This?</h5>
              <h6>We always try to deliver the product as soon as possible</h6>
            </div>
          </div>
        </div>
      </div>
    );
  }
}
