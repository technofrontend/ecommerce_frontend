import React, { Component } from "react";
import DetailSection from "./DetailSection";
import PriceSection from "./PriceSection";
import TitleSection from "./TitleSection";
import "../ProductDesign.css";

export default class ProductDescSection extends Component {
  render() {
    let {
      name,
      info,
      listedP,
      actualP,
      category,
      id,
      review,
      rating,
    } = this.props;

    return (
      <div className="productDesc__body">
        <div className="productDesc__main">
          <TitleSection
            name={name}
            id={id}
            review={review}
            category={category}
            rating={rating}
          />
          <hr />
          <PriceSection actualP={actualP} listedP={listedP} />
          <hr />

          <DetailSection info={info} />
        </div>
      </div>
    );
  }
}
