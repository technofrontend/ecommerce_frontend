import React, { Component } from "react";
import { Link } from "react-router-dom";
import RatingStars from "../../../../../../Shared/Ratings/RatingStars";

export default class TitleSection extends Component {
  render() {
    let { name, id, review, category, rating } = this.props;
    return (
      <div className="productDesc__title">
        <h2>{name}</h2>
        <div className="productDesc__rr">
          <RatingStars id={id} rating={rating} />

          <div className="productDesc__review">
            <h4>
              {review.length === 1
                ? review.length + " rating"
                : review.length + " ratings"}
            </h4>
          </div>
        </div>
        <div className="productDesc__category">
          <div className="productDesc__logo">
            <h4>
              Our <span>Choice</span>
            </h4>
          </div>
          <div className="productDesc__categoryName">
            <h5>
              for{" "}
              <Link to={`/${category.toLowerCase()}/pl`}>
                <span>"{category}"</span>
              </Link>
            </h5>
          </div>
        </div>
      </div>
    );
  }
}
