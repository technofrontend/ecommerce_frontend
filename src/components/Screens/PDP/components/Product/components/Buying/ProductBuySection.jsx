import React, { Component } from "react";
import {
  FacebookShareButton,
  EmailShareButton,
  TwitterShareButton,
  FacebookIcon,
  EmailIcon,
  TwitterIcon,
} from "react-share";
import { withRouter } from "react-router-dom";

import "../ProductDesign.css";
import Button from "../../../../../../Shared/Buttons/Button";
import { AddtoWishlist, AddtoCart } from "../../../../../../Store/fetchCalls";

class ProductBuySection extends Component {
  constructor(props) {
    super(props);
    this.state = {
      disabled: false,
      quant: 1,
    };
  }
  componentDidMount() {
    let { data } = this.props;

    if (data.unitsleft === 0) {
      this.setState({ disabled: true });
    }
  }

  handleChange = (e) => {
    this.setState({ quant: e.target.value });
  };
  render() {
    let {quant}=this.state
    let { path } = this.props.location.pathname;
    let { id } = this.props;
    let { disabled } = this.state;

    return (
      <div className="productBuy__body">
        <div className="productBuy__share">
          <p>Share</p>
          <FacebookShareButton url={path}>
            <FacebookIcon size={24} round={true} />
          </FacebookShareButton>
          <EmailShareButton url={path}>
            <EmailIcon size={24} round={true} />
          </EmailShareButton>
          <TwitterShareButton url={path}>
            <TwitterIcon size={24} round={true} />
          </TwitterShareButton>
        </div>
        <div className="productBuy__main">
          <form>
            <div className="productBuy__form">
              <div className="productBuy__quantity">
                <label>Quantity</label>
                <select name="quantity" onChange={this.handleChange}>
                  <option value="1">1</option>
                  <option value="2">2</option>
                  <option value="3">3</option>
                  <option value="4">4</option>
                  <option value="5">5</option>
                  <option value="6">6</option>
                  <option value="7">7</option>
                  <option value="8">8</option>
                  <option value="9">9</option>
                  <option value="10">10</option>
                </select>
              </div>
              <div className="productBuy__formButtons">
                {/* <Button
                  type={"Submit"}
                  text={"BUY NOW"}
                  className={"form__button bn"}
                  itag={"fas fa-play"}
                  divClass={"form__button__i"}
                  disabled={disabled}
                /> */}
                <Button
                  Click={(e) => {
                    AddtoCart(id,quant, e);
                  }}
                  type={"Submit"}
                  text={"ADD TO CART"}
                  className={"form__button atc"}
                  itag={"fas fa-shopping-cart"}
                  divClass={"form__button__i"}
                  disabled={disabled}
                />
              </div>
              <div className="productBuy__secure">
                <i className="fas fa-lock" />
                <p> Secure Transaction</p>
              </div>
              <hr />
              <Button
                Click={(e) => {
                  AddtoWishlist(id, e);
                }}
                type={"Submit"}
                className={"form__button atw"}
                text={"ADD TO WISHLIST"}
              />
            </div>
          </form>
        </div>
      </div>
    );
  }
}

export default withRouter(ProductBuySection);
