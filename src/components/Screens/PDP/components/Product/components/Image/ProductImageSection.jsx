import React, { Component } from "react";
import "../ProductDesign.css";

export default class ProductImageSection extends Component {
  render() {
    let { src, alt } = this.props;
    return (
      <div className="productImage__body">
        <div className="productImage__main">
          <div className="productImage__left">
            <img className="mini__image" src={src} alt={alt} />
          </div>
          <div className="productImage__right">
            <img className="normal__image" src={src} alt={alt} />
          </div>
        </div>
      </div>
    );
  }
}
