import React, { Component } from "react";
import ProductBuySection from "./components/Buying/ProductBuySection";
import ProductDescSection from "./components/Description/ProductDescSection";
import ProductImageSection from "./components/Image/ProductImageSection";
import "./ProductSection.css";

export default class ProductSection extends Component {
  render() {
    let { data } = this.props;
    return (
      <div className="productSection__body">
        <div className="productSection__main">
          <ProductImageSection src={data.info.imageurl} alt={data.name} />
          <ProductDescSection
            actualP={data.actualprice}
            rating={data.avgrating}
            category={data.category}
            listedP={data.listedprice}
            info={data.info}
            review={data.reviewlist}
            name={data.name}
            id={data.pid}
          />
          <ProductBuySection data={data.info}
          id={data.pid} />
        </div>
      </div>
    );
  }
}
