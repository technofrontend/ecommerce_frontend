import React, { Component } from "react";
import { getSingleProduct } from "../../Store/fetchCalls";
import BodyHeader from "../ProductListPage/components/BodyComponents/BodyHeader";
import ProductSection from "./components/Product/ProductSection";
import ReviewSection from "./components/Review/ReviewSection";

export default class ProductDetail extends Component {
  constructor(props) {
    super(props);
    this.state = {
      data: {},
      names: [],
      fetching: true,
      user: false,
    };
  }

  componentDidMount() {
    console.log(this.props);

    let id = this.props.data.params.pid;

    getSingleProduct(id).then((res) => {
      this.setState({ data: res[0], names: res[1], fetching: false });
    });

    const userData = localStorage.getItem("user");
    if (userData) {
      this.setState({ user: true });
    }
  }

  render() {
    let { data, names, fetching, user } = this.state;

    if (fetching === true) {
      return <h1>Loading</h1>;
    }

    console.log(data);

    return (
      <div className="pdp__body">
        <BodyHeader data={names} />
        <hr />
        {/* Body Product */}
        <ProductSection data={data} />
        <hr />
        {/* Product Review Section */}
        <ReviewSection data={data} user={user} />
      </div>
    );
  }
}
