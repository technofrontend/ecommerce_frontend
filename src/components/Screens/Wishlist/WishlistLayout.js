import React from "react";
import "./WishlistLayout.css";
import Cookies from "universal-cookie";
import DeleteIcon from "@material-ui/icons/Delete";

class WishlistLayout extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      showDescription: false,
    };
  }

  RemoveWishlist = () => {
    const cookie = new Cookies();
    const jwtToken = cookie.get("uuid");
    fetch("https://protected-basin-07111.herokuapp.com/wishlist/update", {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
        Authorization: "Bearer " + jwtToken,
      },
      body: JSON.stringify({
        productid: this.props.wishlistinfo.pid,
        delete: true,
      }),
    }).then((res) => {
      res.json().then((result) => {
        this.props.handleStateChange(result["products"]);
      });
    });
  };

  AddtoCart = () => {
    const cookie = new Cookies();
    const jwtToken = cookie.get("uuid");
    fetch("https://protected-basin-07111.herokuapp.com/user/cart/update-cart", {
      method: "post",
      headers: {
        "Content-type": "application/json;charset=UTF-8",
        Authorization: "Bearer " + jwtToken,
      },
      body: JSON.stringify({
        product: {
          quantity: 1,
          pid: this.props.wishlistinfo.pid,
        },
      }),
    }).then((res) => {
      res.json().then((result) => {});
    });
  };

  AddtoCart_RemoveWishlist = () => {
    this.AddtoCart();

    this.RemoveWishlist();
  };
 
  handleDescriptionChange = () => {
 
    this.setState({
      showDescription: !this.state.showDescription,
    });
    console.log(this.state.showDescription);
  };

  render() {
    const wishlistinfo = this.props.wishlistinfo;

    return (
      <>
        <div id="wishlist-main">
          <div class="wishlist-image" id="">
              <img
                id="wishlist-image"
                alt="something"
                src={wishlistinfo.info.imageurl}
              />
          </div>

          <div className="wishlist-info">
            <p id="product-name">{wishlistinfo.name}</p>
            <p id="description-heading" onClick={this.handleDescriptionChange}> Description ⬇️ </p>
            {(() => {
              if (this.state.showDescription)
                return <p id="description">{wishlistinfo.info.description}</p>;
            })()}
            <div className="price">
              <p className="actual-price">
                Actual Price : ₹{wishlistinfo.actualprice}
              </p>
              <p className="listed-price">
                {" "}
                Discounted Price : ₹{wishlistinfo.listedprice}
              </p>
            </div>

            <div className="wishlist-buttons">
              <button id="add-to-cart" onClick={this.AddtoCart_RemoveWishlist}>
                Add to Cart
              </button>
              <button id="remove-wishlist" onClick={this.RemoveWishlist}>
                <DeleteIcon fontSize="medium" />
              </button>
            </div>

            <div>
              {(() => {
                if (wishlistinfo.unitsleft === 0) {
                  return (
                    <div>
                      <p className="stock"> Out of Stock</p>
                    </div>
                  );
                } else if (wishlistinfo.unitsleft < 10) {
                  return (
                    <div>
                      <p className="stock">Hurry Up! Limited Stock Left</p>
                    </div>
                  );
                } else {
                  return (
                    <div>
                      <p className="stock">In Stock</p>
                    </div>
                  );
                }
              })()}
            </div>
          </div>
        </div>
      </>
    );
  }
}
export default WishlistLayout;
