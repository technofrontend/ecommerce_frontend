import React from "react";

import WishlistLayout from "./WishlistLayout";
import "./WishlistLayout.css";
import Cookies from "universal-cookie";

class WishlistMain extends React.Component {
  constructor() {
    super();
    this.state = { wishlistData: [] };
  }

  componentDidMount() {
    let getWishlist = () => {
      const cookie = new Cookies();
      const jwtToken = cookie.get("uuid");
      try {
        fetch("https://protected-basin-07111.herokuapp.com/wishlist/list", {
          method: "GET",
          headers: {
            "Content-Type": "application/json",
            Authorization: "Bearer " + jwtToken,
          },
        }).then((res) => {
          res.json().then((result) => {
            this.setState({
              wishlistData: result["products"],
            });
          });
        });
      } catch (e) {
        console.log(e);
      }
    };
    getWishlist();
  }

  handleStateChange = (data) => {
    console.log("data from handle changeeeeee");
    console.log(data);
    this.setState({
      wishlistData: data,
    });
  };

  render() {
    let wishlistData = this.state.wishlistData;

    if (wishlistData === undefined || wishlistData === null) {
      return (
        <div>
          <h1>Your Wishlist is Empty</h1>
        </div>
      );
    }

    return (
      <div className="wishlist-main">
        <h1>Wishlist</h1>
        <hr></hr>

        {wishlistData?.map((wishlistinfo) => {
          return (
          
              <WishlistLayout
                handleStateChange={this.handleStateChange}
                wishlistinfo={wishlistinfo}
              />
            
          );
        })}
      </div>
    );
  }
}

export default WishlistMain;
