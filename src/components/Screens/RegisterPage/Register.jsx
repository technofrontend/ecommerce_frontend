import React, { Component } from "react";
import { Link } from "react-router-dom";

import "./Register.css";
import Input from "../../Forms/formComponents/Input";
import Authenticate from "../../Forms/Authenticate/Authenticate";
import Button from "../../Shared/Buttons/Button";
import { RegisterForm } from "../../Store/fetchCalls";

export default class Register extends Component {
  constructor(props) {
    super(props);

    this.state = {
      form: {},
      user: {
        email: "",
        password: "",
        cpassword: "",
        fname: "",
        lname: "",
        phoneno: "",
        street: "",
        city: "",
        country: "",
        pincode: "",
      },
      click: false,
      role: "BUYER",
    };
  }

  componentDidMount() {
    let formData = RegisterForm();

    if (formData) {
      this.setState({ form: formData });
    }
  }

  componentDidUpdate(prevProps) {
    let { user } = this.props;
    if (user !== prevProps.user) {
      this.props.history.push("/");
      window.location.reload();
    }
  }

  handleClick = (context) => {
    let changeClick = this.state.click;

    this.setState({ click: !changeClick, role: context });
  };

  handleChange = (e, context) => {
    let name = e.target.name;
    let value = e.target.value;

    this.setState((prevState) => ({
      user: {
        ...prevState.user,
        [name]: value ? value : context,
      },
    }));
  };

  handleSubmit = (e) => {
    e.preventDefault();
    let user = {
      email: this.state.user.email,
      password: this.state.user.password,
      firstname: this.state.user.fname,
      lastname: this.state.user.lname,
      phoneno: this.state.user.phoneno,
      role: this.state.role,
      address: {
        address: this.state.user.street,
        city: this.state.user.city,
        country: this.state.user.country,
        pincode: this.state.user.pincode,
      },
    };
    Authenticate.registerUser(user);
  };

  render() {
    let { click } = this.state;

    let data = this.state.form.data;
    if (!data) {
      return <h4>Loading</h4>;
    }

    let fields = data?.formData?.sections[0].fields;
    return (
      <div className="form_register">
        <h1>Create Account</h1>
        <div className="form_registerOptions">
          <Link to={{ pathname: "/register" }}>
            <button
              className={click ? "register_button" : "register_button active"}
              onClick={() => {
                this.handleClick("BUYER");
              }}
            >
              Buyer
            </button>
          </Link>
          <Link to={{ pathname: "/register", state: this.state.user }}>
            <button
              className={click ? "register_button active" : "register_button"}
              onClick={() => {
                this.handleClick("SELLER");
              }}
            >
              Seller
            </button>
          </Link>
        </div>
        <div className="form_registerContainer">
          <form>
            <div className="form_register_inner">
              {fields &&
                fields.map((field, i) => {
                  const inpData = data?.fieldsData[field];
                  if (!inpData) {
                    return false;
                  }
                  return (
                    <Input
                      key={i}
                      data={inpData}
                      action={this.handleChange}
                      value={this.state.user.name}
                    />
                  );
                })}
            </div>

            <div>
              <Button
                type={"Submit"}
                text={"Submit"}
                className={"yellowButton"}
                Click={this.handleSubmit}
              />
            </div>
          </form>
        </div>
      </div>
    );
  }
}
