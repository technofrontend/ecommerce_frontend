import React from "react";
import ProductList from "./ProductListCart";
import Cookies from "universal-cookie";
import "./Cart.css";
import {Link} from "react-router-dom"
// import ShoppingCartIcon from "@material-ui/icons/ShoppingCart";

class CartLayout extends React.Component {
  constructor(props) {
    super(props);
    this.handleStateChange = this.handleStateChange.bind(this);
    this.state = {
      cartData: [],
      amount: "",
    };
  }

  componentDidMount() {
    // let getCart = () => {
      const cookie = new Cookies();
      const jwtToken = cookie.get("uuid");
    try {
      fetch("https://protected-basin-07111.herokuapp.com/user/cart/get-cart", {
        method: "GET",
        headers: {
          "Content-Type": "application/json",
          Authorization: "Bearer " + jwtToken,
        },
      }).then((res) => {
        res.json().then((result) => {
          console.log("this is the response from cart api");
          this.setState({
            cartData: result["allproducts"],
            amount: result.amount,
          });
        });
      });
    } catch (e) {
      console.log(e);
    }
  }
  //   getCart();
  // }

  handleStateChange = (data, amount) => {
    this.setState({
      cartData: data,
      amount: amount,
    });
  };

  render() {
    let cartdata = this.state.cartData;
    console.log(this.state.carData);
    if (this.state.cartData === undefined || this.state.cartData === null) {
      console.log("enbjdbjdb");
      return (
        <div>
          <h1>Your Cart is Empty</h1>
        </div>
      );
    }
    return (
      <>
        <div className="cartmain">
          <h1>Cart</h1>
          <hr/>
        </div>
        <div className="cartlayout">
          <div className="ProductList">
            {cartdata?.map((product, index) => {
              console.log("this is classlayout product");
              console.log(product);
              return (
                <ProductList
                  handleStateChange={this.handleStateChange}
                  index={index}
                  product={product}
                />
              );
            })}
          </div>
          <div className="ordersummary">
            <h2>Order Summary</h2>
            <p>Order Total : ₹{this.state.amount}</p>
            <Link to="/orderReview">

            <button id="checkout" > Proceed to Checkout</button>
            </Link>
          </div>
        </div>
      </>
    );
  }
}
export default CartLayout;
