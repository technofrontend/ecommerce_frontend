import React from "react";
import "./Cart.css";
import Counter from "../Cart/Components/Counter/Counter"
class ProductList extends React.Component {
  render() {
    let data = this.props.product.product;

    if (data === undefined) {
      return <div>Nodata</div>;
    }

    return (
      <>
        <div id="main">
          <div class="gallery" id="">
              <img
                id="product-image"
                alt="something"
                src={data.info.imageurl}
              />
          </div>
          <div className="info">
            <p id="description">{data.name}</p>
            <div className="price">
              <p className="actual-price">Actual Price : ₹{data.actualprice}</p>
              <p className="listed-price">
                Discounted Price : ₹{data.listedprice}
              </p>
            </div>

            <Counter
              handleStateChange={this.props.handleStateChange}
              quantity={this.props.product.quantity}
              pid={data.pid}
              actualprice={data.actualprice}
              price={data.listedprice}
              unitsleft={data.info.unitsleft}
            />

            {(() => {
              if (data.info.unitsleft === 0) {
                return (
                  <div>
                    <p className="stock"> Out of Stock</p>
                  </div>
                );
              } else if (data.info.unitsleft < 10) {
                return (
                  <div>
                    <p className="stock">Hurry Up! Limited Stock Left</p>
                  </div>
                );
              } else {
                return (
                  <div>
                    <p className="stock">In Stock</p>
                  </div>
                );
              }
            })()}
          </div>
        </div>
      </>
    );
  }
}
export default ProductList;
