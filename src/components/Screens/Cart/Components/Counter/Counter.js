import React from "react";
import DeleteIcon from "@material-ui/icons/Delete";
import "./Counter.css";
import Cookies from "universal-cookie";

class Counter extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      count: this.props.quantity,
    };
  }

  componentDidMount() {
    if (this.props.quantity > this.props.unitsleft) {
      this.setState({
        count: this.props.unitsleft,
      });
    }
  }

  UpdateCart = (quant) => {
    const cookie = new Cookies();
    const jwtToken = cookie.get("uuid");
    try {
      fetch(
        "https://protected-basin-07111.herokuapp.com/user/cart/update-cart",
        {
          method: "post",
          headers: {
            "Content-type": "application/json;charset=UTF-8",
            Authorization: "Bearer " + jwtToken,
          },
          body: JSON.stringify({
            product: {
              quantity: quant,
              pid: this.props.pid,
            },
          }),
        }
      ).then((res) => {
        res.json().then((result) => {
          this.props.handleStateChange(result["allproducts"], result.amount);
        });
      });
    } catch (e) {
      console.log(e);
    }
  };

  IncNum = () => {
    if (this.state.count < 10 && this.state.count < this.props.unitsleft) {
      this.setState({
        count: this.state.count + 1,
      });
      setTimeout(() => {
        this.UpdateCart(this.state.count);
      }, 100);
    }
  };

  DecNum = () => {
    if (this.state.count !== 1 && this.state.count !== 0) {
      this.setState({
        count: this.state.count - 1,
      });
      setTimeout(() => {
        this.UpdateCart(this.state.count);
      }, 100);
    }
  };

  Delete = () => {
    this.setState({
      count: 0,
    });
    setTimeout(() => {
      this.UpdateCart(this.state.count);
    }, 1000);
  };

  render() {
    return (
      <>
        <div className="quantity">
          <h4 id="quant"> Qty : {this.state.count}</h4>
        </div>

        <div className="counter-button">
          <button className="incDec" onClick={this.DecNum}>
            -
          </button>
          <button className="incDec" onClick={this.IncNum}>
            +
          </button>
          <button className="deletecart" onClick={this.Delete}>
            <DeleteIcon fontSize="small" />
          </button>
        </div>

        <div id="cart">
          <p id="you-save">
            You Save: ₹
            {this.state.count * (this.props.actualprice - this.props.price)}
          </p>
          <p id="item-total">
            Item Total : ₹{this.state.count * this.props.price}
          </p>
        </div>
      </>
    );
  }
}
export default Counter;
