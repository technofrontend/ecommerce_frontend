import React from "react";

class FlashMessage extends React.Component {
  constructor(props) {
    super(props);
    this.state = { isVisible: true };
    this.hide = this.hide.bind(this);
  }

  hide() {
    this.setState({ isVisible: false });
  }

  componentDidMount() {
    const { duration } = this.props;
    setTimeout(this.hide, duration);
  }

  render() {
    const { isVisible } = this.state;
    const { children } = this.props;
    return isVisible ? (
      <div
        id="flash"
        className={`flash ${isVisible ? "flash-visible": ""}`}
      >
        {children}
      </div>
    ) : null;
  }
}

export default FlashMessage;
