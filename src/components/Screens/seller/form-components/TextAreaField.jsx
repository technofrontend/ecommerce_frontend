import React from "react";

class TextAreaField extends React.Component {
  render() {
    const { label, placeholder, name, value, handler, isRequired,errorMsg } = this.props;
    return (
      <div className="product-form-field">
        <label className="label-input" htmlFor={name}>
          {label}
        </label>
        <textarea
          className="input-form-field"
          rows={5}
          id={name}
          name={name}
          placeholder={placeholder}
          value={value}
          onChange={handler}
          required={isRequired === "true" ? true : false}
        />
        <div className="errorMsg">{errorMsg}</div>
      </div>
    );
  }
}

export default TextAreaField;
