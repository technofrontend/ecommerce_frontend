import React from "react";
import InputFormField from "./InputFormField";
import SingleSelectFormField from "./SingleSelectFormField";
import TextAreaField from "./TextAreaField";
import Button from "./Button";
import ImageUpload from "./ImageUpload";
import ToggleSwitch from "./ToggleSwitch";
import FlashMessage from "./FlashMessage";
import axios from "axios";
import { withRouter } from 'react-router-dom';

class ProductForm extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      product: {
        name: "",
        category: "",
        actualprice: "",
        listedprice: "",
        visibilty: false,
        imageurl: "",
        description: "",
        unitsleft: "",
      },
      type: "",
      categoryOptions: [],
      errors: {},
      isSuccess: true
    };
    this.handleChange = this.handleChange.bind(this);
    this.handleImgUpload = this.handleImgUpload.bind(this);
    this.validateForm = this.validateForm.bind(this);
  }

  handleChange = (event) => {
    let name = event.target.name;
    let value = event.target.value;

    this.setState((prevState) => ({
      product: {
        ...prevState.product,
        [name]: value ? value : "",
      },
    }));
  };

  handleToggleSwitch = (event, check) => {
    this.setState((prevState) => ({
      product: {
        ...prevState.product,
        visibilty: !check,
      },
    }));
  };

  handleImgUpload = (event) => {
    this.setState((prevState) => ({
      product: {
        ...prevState.product,
        imageurl: event.target.src,
      },
    }));
  };

  fetchCategories = (product) => {
    const HOST = "https://protected-basin-07111.herokuapp.com";
    axios
      .get(`${HOST}/product/getcategories`)
      .then((response) => {
        this.setState({ categoryOptions: response.data });
      })
      .catch((error) => {
        console.log(error.message);
      });
  };

  isValidImageURL = (url, timeout) =>
    new Promise((res) => {
      timeout = timeout || 5000;
      let timedOut = false;
      let timer;
      const img = new Image();

      img.onerror = img.onabort = function () {
        if (!timedOut) {
          clearTimeout(timer);
          res("error");
        }
      };

      img.onload = function () {
        if (!timedOut) {
          clearTimeout(timer);
          res("success");
        }
      };

      img.src = url;

      timer = setTimeout(function () {
        timedOut = true;
        // reset .src to invalid URL so it stops previous
        // loading, but doesn't trigger new load
        img.src = "//!!!!/test.jpg";
        res("timeout");
      }, timeout);
    });

  async validateForm() {
    let formfields = this.state.product;
    let errors = {};
    let isFormValid = true;

    if (formfields["name"] === "") {
      isFormValid = false;
      errors["name"] = "* Please Enter a Name.";
    }

    if (formfields["category"] === "") {
      isFormValid = false;
      errors["category"] = "* Please Select a Category";
    }

    if (formfields["actualprice"] === "") {
      isFormValid = false;
      errors["actualprice"] = "* Please Enter Actual Price";
    }

    if (formfields["listedprice"] === "") {
      isFormValid = false;
      errors["listedprice"] = "* Please Enter Listed Price";
    }

    if (formfields["unitsleft"] === "") {
      isFormValid = false;
      errors["unitsleft"] = "* Please Enter Quantity";
    }

    if (formfields["description"] === "") {
      isFormValid = false;
      errors["description"] = "* Please Enter a Description.";
    }

    if (formfields["imageurl"] === "") {
      isFormValid = false;
      errors["imageurl"] = "* Please Enter an Image URL";
    } else {
      let result = await this.isValidImageURL(
        formfields["imageurl"],
        1000
      ).then((status) => status === "success");
      if (!result) {
        isFormValid = false;
        errors["imageurl"] = "* Please Enter an valid Image URL";
      }
    }

    this.setState({ errors: errors });
    return isFormValid;
  }

  onSubmitForm = async (event) => {
    event.preventDefault();
    this.setState({ isSuccess: true });
    if (await this.validateForm()) {
      this.props
        .onSubmit(this.state.product)
        .then((response) => response.json()) // convert to json
        .then((data) => {
          if (data) {
            this.props.history.push("/seller/products");
            console.log(this.props);
          }
        }).catch((error) => {
          console.log("catch called");
          console.log(error.message);
          this.setState({ isSuccess: false });
        });
    }
  };

  componentDidMount() {
    this.setState({ product: this.props.product, type: this.props.type });
    this.fetchCategories();
  }

  render() {
    return (
      <>
      {this.state.isSuccess === false && (
          <FlashMessage duration={3000}>
            <strong>Failed! Some Error Occured</strong>
          </FlashMessage>
        )}
        <form noValidate className="product-form" onSubmit={this.onSubmitForm}>
          <div className="product-form-wrapper">
            <div className="product-form-inputs">
              <div className="flex-row">
                <InputFormField
                  type="text"
                  label="* Product Name"
                  name="name"
                  placeholder="Enter Product Name"
                  value={this.state.product.name}
                  handler={this.handleChange}
                  formtype={this.state.type}
                  isRequired="true"
                  errorMsg={this.state.errors.name}
                />
                <SingleSelectFormField
                  name="category"
                  id="category"
                  options={this.state.categoryOptions}
                  label="* Category"
                  value={this.state.product.category}
                  handler={this.handleChange}
                  formtype={this.state.type}
                  isRequired="true"
                  errorMsg={this.state.errors.category}
                />
              </div>
              <div className="flex-row">
                <TextAreaField
                  label="*  Description"
                  name="description"
                  placeholder="Enter Product Description"
                  value={this.state.product.description}
                  handler={this.handleChange}
                  isRequired="true"
                  errorMsg={this.state.errors.description}
                />
              </div>
              <div className="flex-row">
                <InputFormField
                  type="number"
                  label="* Actual Price"
                  name="actualprice"
                  placeholder="Enter Actual Price"
                  value={this.state.product.actualprice}
                  handler={this.handleChange}
                  isRequired="true"
                  errorMsg={this.state.errors.actualprice}
                />
                <InputFormField
                  type="number"
                  label="* Listed Price"
                  name="listedprice"
                  placeholder="Enter Listed Price"
                  value={this.state.product.listedprice}
                  handler={this.handleChange}
                  isRequired="true"
                  errorMsg={this.state.errors.listedprice}
                />
                <InputFormField
                  type="number"
                  label="* Quantity"
                  name="unitsleft"
                  placeholder="Enter Quantity"
                  value={this.state.product.unitsleft}
                  handler={this.handleChange}
                  isRequired="true"
                  errorMsg={this.state.errors.unitsleft}
                />
              </div>
              <div className="flex-row">
                <InputFormField
                  type="text"
                  label="* Image URL"
                  name="imageurl"
                  placeholder="Enter Image URL"
                  value={this.state.product.imageurl}
                  handler={this.handleChange}
                  isRequired="true"
                  errorMsg={this.state.errors.imageurl}
                />
                <ToggleSwitch
                  label="* Visibilty"
                  name="visibilty"
                  value={this.state.product.visibilty}
                  handler={this.handleToggleSwitch}
                />
              </div>
            </div>
            <div className="product-form-img-upload">
              <div className="flex-col">
                <ImageUpload
                  name="image"
                  imageurl={this.state.product.imageurl}
                  handler={this.handleImgUpload}
                />
              </div>
            </div>
          </div>
          <div className="flex-row">
            <Button btnLabel="Save Product" />
          </div>
        </form>
      </>
    );
  }
}

export default withRouter(ProductForm);
