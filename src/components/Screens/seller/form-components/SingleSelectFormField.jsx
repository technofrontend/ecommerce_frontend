import React from "react";

class SingleSelectFormField extends React.Component {
  render() {
    const { label, options, name, value, handler, formtype, isRequired, errorMsg } = this.props;
    return (
      <div className="product-form-field">
        <label className="label-input" htmlFor={name}>
          {label}
        </label>
        <select
          className="select-form-field"
          name={name}
          id={name}
          value={value}
          onChange={handler}
          disabled={formtype === "edit" ? true : false}
          required={isRequired === "true" ? true : false}
        >
          <option key="default-option" value="" disabled>
            Choose Category
          </option>
          {options.map((category, i) => (
            <option key={i} value={category}>
              {category}
            </option>
          ))}
        </select>
        <div className="errorMsg">{errorMsg}</div>
      </div>
    );
  }
}

export default SingleSelectFormField;
