import React from "react";
import storage from "../../../Store/Firebase/index";

class ImageUpload extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      url: null,
      progress: 0,
      image: null,
    };
    this.handleChange = this.handleChange.bind(this);
  }

  handleChange(event) {
    if (event.target.files[0]) {
      const image = event.target.files[0];
      this.setState(() => ({ image }));

      var randomImgId = 10000 + Math.random() * (9999999 - 10000);

      const uploadTask = storage
        .ref(`images/${randomImgId + image.name}`)
        .put(image);

      uploadTask.on(
        "state_changed",
        (snapshot) => {
          // progress function
          const progress = Math.round(
            (snapshot.bytesTransferred / snapshot.totalBytes) * 100
          );
          this.setState({ progress });
        },
        (error) => {
          // Error function
          console.log(error);
        },
        () => {
          // Get URL From Firebase
          storage
            .ref("images")
            .child(randomImgId + image.name)
            .getDownloadURL()
            .then((url) => {
              this.setState({ url });
            });
        }
      );
    }
  }

  render() {
    const { name, handler, imageurl } = this.props;
    return (
      <>
        <div className="product-form-img-field">
          <label className="label-input" htmlFor={name}>
            *Thumbnail
          </label>
          <label className="label-img-upload" htmlFor={name}>
            <input
              hidden
              type="file"
              accept="image/*"
              name={name}
              id={name}
              onChange={this.handleChange}
            />
            Upload Image
          </label>
        </div>
        <div className="product-form-img-field img-upload-wrapper">
          {(this.state.url || imageurl) && (
            <img
              className="img-preview"
              alt=""
              src={this.state.url || imageurl}
              onLoad={handler}
            />
          )}
        </div>
      </>
    );
  }
}

export default ImageUpload;
