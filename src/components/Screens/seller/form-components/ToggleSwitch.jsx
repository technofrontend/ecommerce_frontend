import React from "react";

class ToggleSwitch extends React.Component {
  render() {
    const { label, name, value, handler } = this.props;
    return (
      <div className="product-form-field toggle-switch-container">
        <label className="label-input" htmlFor={name}>
          {label}
        </label>
        <label className="switch">
          <input
            className="toggle-form-field"
            type="checkbox"
            id={name}
            name={name}
            onChange={(event) => {handler(event, value)}}
            checked = {value}
          />
          <span className="slider round" />
        </label>
      </div>
    );
  }
}

export default ToggleSwitch;
