import React from "react";

class Button extends React.Component {
  render() {
      const { btnLabel } = this.props
    return (
      <div className="product-form-field">
        <button className="button" type="submit">{btnLabel}</button>
      </div>
    );
  }
}

export default Button;
