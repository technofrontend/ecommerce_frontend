import React from "react";

class FileUpload extends React.Component {
  render() {
    const { name, handler } = this.props;
    return (
      <div className="product-form-field">
        <label className="label-input" htmlFor={name}>
          * Bulk Upload
        </label>
        <label className="label-file-upload" htmlFor={name}>
          <input
            hidden
            type="file"
            accept="application/json"
            name={name}
            id={name}
            onChange={handler}
          />
          Browse file
        </label>
      </div>
    );
  }
}

export default FileUpload;