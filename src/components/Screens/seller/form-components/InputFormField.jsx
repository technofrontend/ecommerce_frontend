import React from "react";

class InputFormField extends React.Component {
  render() {
    const { label, type, placeholder, name, value, handler, formtype, isRequired,errorMsg } = this.props;
    return (
      <div className="product-form-field">
        <label className="label-input" htmlFor={name}>
          {label}
        </label>
        <input
          className="input-form-field"
          type={type}
          id={name}
          name={name}
          placeholder={placeholder}
          value={value}
          disabled={formtype === "edit" ? true : false}
          required={isRequired === "true" ? true : false}
          min="0"
          onChange={handler}
        />
        <div className="errorMsg">{errorMsg}</div>
      </div>
    );
  }
}

export default InputFormField;
