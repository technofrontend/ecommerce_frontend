import React from "react";
import FileUpload from "./FileUpload";
import Button from "./Button";
import Cookies from "universal-cookie";
import { withRouter } from "react-router-dom";

class BulkUploadForm extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      products: [],
    };
    this.onSubmitForm = this.onSubmitForm.bind(this);
    this.handleFileUpload = this.handleFileUpload.bind(this);
  }

  handleFileUpload = (event) => {
    if (event.target.files[0]) {
      const file = event.target.files[0];
      let fileReader = new FileReader();
      fileReader.onload = (event) => {
        this.setState({ products: JSON.parse(event.target.result) });
      };
      fileReader.readAsText(file);
    }
  };

  onSubmitForm = (event) => {
    let cookies = new Cookies();
    event.preventDefault();
    const HOST = "https://protected-basin-07111.herokuapp.com";
    const jwtToken = cookies.get("uuid");
    console.log(jwtToken);
    fetch(`${HOST}/product/bulk-save`, {
      method: "post",
      headers: {
        "Content-type": "application/json;charset=UTF-8",
        Accept: "application/json;charset=UTF-8",
        Authorization: "Bearer " + jwtToken,
      },
      body: JSON.stringify(this.state.products),
    })
      .then((response) => response.json()) // convert to json
      .then((data) => {
        console.log(data);
        if (data) {
          this.props.history.push("/seller/products");
          window.location.reload();
        }
      });
  };

  render() {
    return (
      <>
        <form className="product-form" onSubmit={this.onSubmitForm}>
          <div className="product-form-wrapper">
            <FileUpload name="bulkupload" handler={this.handleFileUpload} />
          </div>
          <Button btnLabel="Save Products" />
        </form>
      </>
    );
  }
}

export default withRouter(BulkUploadForm);
