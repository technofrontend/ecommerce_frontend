import React from "react";
import ProductForm from "../form-components/ProductForm";
import "../../../../styles/form-components-styles/ProductForm.css";
import "../../../../styles/form-components-styles/Button.css";
import "../../../../styles/form-components-styles/ToggleSwitch.css";
import Cookies from "universal-cookie";
import { Link } from "react-router-dom";

class AddProduct extends React.Component {
  onSubmit = async (product) => {
    let cookies = new Cookies();
    const jwtToken = cookies.get("uuid");
    const HOST = "https://protected-basin-07111.herokuapp.com";
    const response = await fetch(`${HOST}/product/save`, {
      method: "post",
      headers: {
        "Content-type": "application/json;charset=UTF-8",
        "Accept": "application/json;charset=UTF-8",
        "Authorization": "Bearer " + jwtToken,
      },
      body: JSON.stringify({
        name: product.name,
        category: product.category,
        actualprice: product.actualprice,
        listedprice: product.listedprice,
        visibilty: product.visibilty,
        info: {
          imageurl: product.imageurl,
          description: product.description,
          unitsleft: product.unitsleft,
        },
      }),
    });
    return response;
  };

  render() {
    return (
      <div className="product-form-container">
        <div className="form-tabs">
          <Link to="/seller/addproduct" className="form-tab-link link-active">
            <h1 className="form-tabs-item">Add Product</h1>
          </Link>
          <Link to="/seller/bulkupload" className="form-tab-link">
            <h1 className="form-tabs-item">Bulk Upload</h1>
          </Link>
        </div>
        <ProductForm
          onSubmit={this.onSubmit}
          type="add"
          product={{
            name: "",
            category: "",
            actualprice: "",
            listedprice: "",
            visibilty: false,
            imageurl: "",
            description: "",
            unitsleft: "",
          }}
        />
      </div>
    );
  }
}

export default AddProduct;
