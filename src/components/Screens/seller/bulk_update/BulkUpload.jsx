import React from "react";
import BulkUploadForm from "../form-components/BulkUploadForm";
import { Link } from 'react-router-dom';

class BulkUpload extends React.Component {
  render() {
    return (
      <div className="product-form-container bg-pattern">
        <div className="form-tabs">
        <Link to="/seller/addproduct" className="form-tab-link"><h1 className="form-tabs-item">Add Product</h1></Link>
        <Link to="/seller/bulkupload" className="form-tab-link link-active"><h1 className="form-tabs-item">Bulk Upload</h1></Link>
        </div>
        <BulkUploadForm />
      </div>
    );
  }
}

export default BulkUpload;
