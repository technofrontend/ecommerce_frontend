import React from "react";
import PropType from "prop-types";
import ProductItem from "./ProductItem";
import AddProductBtn from "./AddProductBtn";
import LoadingSpinner from "./LoadingSpinner";
import "../../../../styles/products-styles/ProductItem.css";

class ProductsGrid extends React.Component {
  render() {
    const { productsList, fetching, deleteProductHandler } = this.props;
    return (
      <div className="bg-pattern">
        <div className="container">
          <main className="grid" id="products">

            {/* If fetching, render Spinner else Show Products */}

            { fetching ? <LoadingSpinner /> : 

              // if Products List empty show No Products Found
              
              (productsList.length > 0 ? (productsList.map(product => (
                  <ProductItem key={`prod-${product.pid}`} product={product} deleteProductHandler={deleteProductHandler} />
                ))) : <h2> No Products Found</h2>)
            }
          </main>
        </div>
        <AddProductBtn />
      </div>
    );
  }
}

ProductsGrid.propTypes = {
  productsList: PropType.array.isRequired,
};

export default ProductsGrid;
