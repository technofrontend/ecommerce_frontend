import React from "react";
import "../../../../styles/products-styles/addproductbtn.css";
import AddRoundedIcon from "@material-ui/icons/AddRounded";
import { Link } from "react-router-dom";

class AddProductBtn extends React.Component {
  render() {
    return (
      <Link to="/seller/addproduct">
        <button id="btn--add-product">
          <AddRoundedIcon />
        </button>
      </Link>
    );
  }
}

export default AddProductBtn;
