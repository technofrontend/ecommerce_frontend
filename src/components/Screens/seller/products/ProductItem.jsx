import React from "react";
import { Link } from "react-router-dom";
import { withRouter } from "react-router-dom";

class ProductItem extends React.Component {
  
  rating = () => {
    const starTotal = 5;
    const ratings = this.props.product.avgrating;
    const starValue = (ratings / starTotal) * 100;
    const starPercentage = `${(starValue / 10) * 10}%`;

    document.getElementById(`star-${this.props.product.pid}`).style.width = starPercentage;
  };

  componentDidMount() {
    this.rating();
  }


  render() {
    const { product, deleteProductHandler } = this.props;
    return (
      <article>
        <div className="product-img-container">
          <img
            className="product-img"
            src={product.info.imageurl}
            alt="{product.name}"
          />
        </div>
        <div className="product-details-container">
          <div className="product-name-wrapper">
            <p className="product-name">{product.name}</p>
          </div>
          <div className="rating-container">
            <Link to="" className="rating-container-link">
              <div className="stars-outer">
                <i className="far fa-star" />
                <div className="stars-inner" id={`star-${product.pid}`}>
                  <i className="fa fa-star" />
                </div>
              </div>
              <div className="rating-text-container"><p className="rating-text">{product.avgrating}/5</p></div>
            </Link>
          </div>
          <div className="products-price-container">
            <div className="listed-price"><span>₹ {product.listedprice}</span></div>
            <div className="actual-price"><span><strike>₹ {product.actualprice} </strike></span></div>
          </div>
          <div className="products-units-container">
            <div className="units-sold"><i className="fas fa-shipping-fast"></i><p>{product.info.unitssold}</p></div>
            <div className="units-left"><i className="fas fa-cubes"></i><p>{product.info.unitsleft}</p></div>
          </div>
        </div>
        <div className="product-buttons">
          <Link
            to={{
              pathname: "/seller/editproduct",
              product: product,
            }}
          >
            <button className="prod-price-btn">Edit</button>
          </Link>
          <button className="prod-price-btn" onClick={(e) => {deleteProductHandler(e,product.pid)}}>
            Delete
          </button>
        </div>
      </article>
    );
  }
}

export default withRouter(ProductItem);
