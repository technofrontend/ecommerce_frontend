import React from "react";
import ProductsGrid from "./ProductsGrid";
import axios from "axios";
import Cookies from "universal-cookie";
import { withRouter } from 'react-router-dom';

class Products extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      products: [],
      fetching: true,
    };
    this.deleteProduct = this.deleteProduct.bind(this);
  }

  componentDidMount() {
    // console.log(this.props);
    // let { userInfo } = this.props;
    // if (userInfo && userInfo.role !== "S") {
    //   this.props.history.push("/");
    // }

    let cookies = new Cookies();
    const HOST = "https://protected-basin-07111.herokuapp.com";
    const jwtToken = cookies.get("uuid");

    axios
      .get(`${HOST}/product/list`, {
        headers: {
          Authorization: "Bearer " + jwtToken,
        },
      })
      .then((response) => {
        this.setState({ products: response.data, fetching: false });
      })
      .catch((error) => {
        console.log(error.message);
        this.setState({ fetching: false });
      });
  }

  deleteProduct(e, pid) {
    let cookies = new Cookies();

    const HOST = "https://protected-basin-07111.herokuapp.com";
    const jwtToken = cookies.get("uuid");
    fetch(`${HOST}/product/delete`, {
      method: "post",
      headers: {
        "Content-type": "application/json;charset=UTF-8",
        Accept: "application/json;charset=UTF-8",
        Authorization: "Bearer " + jwtToken,
      },
      body: JSON.stringify({
        pid: pid,
      }),
    })
      .then((response) => response.json()) // convert to json
      .then((data) => {
        if (data) {
          console.log(data);
          this.setState({ products: data });
          // console.log(this.props);
          // this.props.history.push("/seller/products");
          // // window.location.reload();
        }
      });
  }

  render() {
    return (
      <div>
        <ProductsGrid
          fetching={this.state.fetching}
          productsList={this.state.products}
          deleteProductHandler={this.deleteProduct}
        />
      </div>
    );
  }
}

export default withRouter(Products);
