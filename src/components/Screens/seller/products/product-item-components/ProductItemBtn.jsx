import React from "react";

class ProductItemBtn extends React.Component {
  render() {
    const { label, handler } = this.props;
    return (
      <button className="prod-price-btn" onClick={handler}>
        {label}
      </button>
    );
  }
}

export default ProductItemBtn;
