import React from "react";
import ProductForm from "../form-components/ProductForm";
import "../../../../styles/form-components-styles/ProductForm.css";
import "../../../../styles/form-components-styles/Button.css";
import "../../../../styles/form-components-styles/ToggleSwitch.css";
import Cookies from "universal-cookie";
import { Redirect } from 'react-router-dom';

class EditProduct extends React.Component {
  onSubmit = async (product) => {
    let cookies = new Cookies();
    const HOST = "https://protected-basin-07111.herokuapp.com";
    const jwtToken = cookies.get("uuid");
    product.pid = this.props.location.product.pid;
    const prodData = {
      info: {
        imageurl: product.imageurl,
        description: product.description,
        unitsleft: product.unitsleft,
      },
      pid: product.pid,
      actualprice: product.actualprice,
      listedprice: product.listedprice,
      name: product.name,
      category: product.category,
      visibilty: product.visibilty,
    };

    const response = await fetch(`${HOST}/product/update`, {
      method: "post",
      headers: {
        "Content-type": "application/json;charset=UTF-8",
        Accept: "application/json;charset=UTF-8",
        Authorization: "Bearer " + jwtToken,
      },
      body: JSON.stringify(prodData),
    });
    return response;
  };

  componentDidMount() {
    let { userInfo } = this.props;
    if (userInfo && userInfo.role !== "S") {
      this.props.history.push("/");
    }
  }

  render() {
    if (!this.props.location.product) {
      return <Redirect to="/seller/products" />;
    }
    const productRawData = this.props.location.product;
    const product = {
      name: productRawData.name || "",
      category: productRawData?.category || "",
      description: productRawData?.info.description || "",
      imageurl: productRawData?.info.imageurl || "",
      unitsleft: productRawData?.info.unitsleft || "",
      actualprice: productRawData?.actualprice || "",
      listedprice: productRawData?.listedprice || "",
      visibilty: productRawData?.visibilty || true,
    };

    return (
      <div className="product-form-container">
        <h1 className="form-heading">Edit Product</h1>
        {product && (
          <ProductForm product={product} type="edit" onSubmit={this.onSubmit} />
        )}
      </div>
    );
  }
}

export default EditProduct;
