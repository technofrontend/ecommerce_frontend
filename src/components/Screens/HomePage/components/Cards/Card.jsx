import React, { Component } from "react";

import CardItem from "./CardItem";
import "./Card.css";

export default class Card extends Component {
  render() {
    let { data, fetching } = this.props;

    if (fetching === true) {
      return <h1>Loading</h1>;
    }
    return (
      <div className="cards">
        <h1>Check Out These Categories</h1>
        <div className="cards__container">
          <div className="cards__items">
            {data &&
              data.map((item, index) => {
                return (
                  <CardItem
                    key={index}
                    src={item.image}
                    text={item.text}
                    alt={item.alt}
                    label={item.label}
                    path={item.path}
                  />
                );
              })}
          </div>
        </div>
      </div>
    );
  }
}
