import React, { Component } from "react";
import { Link } from "react-router-dom";

export default class CardItem extends Component {
  render() {
    let { text, src, label, path, alt, data } = this.props;
    return (
      <>
        <div className="cards__item">
          <Link
            className="cards__item__link"
            to={{ pathname: path, data: data }}
          >
            <figure className="cards__item__pic-wrap" data-category={label}>
              <img src={src} alt={alt} className="cards__item__img" />
            </figure>
            <div className="cards__item__info">
              <h5 className="cards__item__text">{text}</h5>
            </div>
          </Link>
        </div>
      </>
    );
  }
}
