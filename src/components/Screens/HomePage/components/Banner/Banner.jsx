import React, { Component } from "react";
import { ArrowBackIos, ArrowForwardIos } from "@material-ui/icons";

import "./Banner.css";

export default class Banner extends Component {
  constructor(props) {
    super(props);
    this.state = {
      current: 0,
    };
  }

  nextSlide = (length) => {
    let { current } = this.state;
    let counter = current === length - 1 ? 0 : current + 1;
    this.setState({ current: counter });
  };

  prevSlide = (length) => {
    let { current } = this.state;
    let counter = current === 0 ? length - 1 : current - 1;
    this.setState({ current: counter });
  };

  render() {
    let { data, fetching } = this.props;

    let length = data.length;
    let { current } = this.state;

    if (!Array.isArray(data) || data.length <= 0) {
      return null;
    }

    if (fetching === true) {
      return <h1>Loading</h1>;
    }

    return (
      <section className="banner">
        <ArrowBackIos
          className="banner_back"
          onClick={() => {
            this.prevSlide(length);
          }}
          fontSize="large"
        />
        <ArrowForwardIos
          className="banner_forward"
          onClick={() => {
            this.nextSlide(length);
          }}
          fontSize="large"
        />

        {data.map((item, index) => {
          return (
            <div
              key={index}
              className={index === current ? "slide active" : "slidex"}
            >
              {index === current && (
                <img
                  src={item.image_H}
                  alt={item.alt}
                  className="banner_image"
                />
              )}
            </div>
          );
        })}
      </section>
    );
  }
}
