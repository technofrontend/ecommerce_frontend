import React, { Component } from "react";

import { GetCategoryBannerData } from "../../Store/fetchCalls";
import Banner from "./components/Banner/Banner";
import Card from "./components/Cards/Card";
import "./Homepage.css";

export default class HomePage extends Component {
  constructor(props) {
    super(props);
    this.state = {
      categories: [],
      banners: [],
      fetching: true,
    };
  }

  componentDidMount() {
    GetCategoryBannerData().then((res) => {
      this.setState({ categories: res[0], banners: res[1], fetching: false });
    });
  }

  render() {
    let { categories, banners, fetching } = this.state;
    return (
      <div className="Homepage">
        <div className="Homepage__banner">
          <Banner data={banners} fetching={fetching} />
        </div>
        <div className="Homepage__cards">
          <Card data={categories} fetching={fetching} />
        </div>
      </div>
    );
  }
}
