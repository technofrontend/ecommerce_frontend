import React, { Component } from "react";
import { Link } from "react-router-dom";

import Input from "../../Forms/formComponents/Input";
import "./Login.css";
import Authenticate from "../../Forms/Authenticate/Authenticate";
import Button from "../../Shared/Buttons/Button";
import { LoginForm } from "../../Store/fetchCalls";

export default class Login extends Component {
  constructor(props) {
    super(props);
    this.handleChange = this.handleChange.bind(this);

    this.state = {
      form: {},
      email: "",
      password: "",
      message: "",
    };
  }

  componentDidMount() {
    let formData = LoginForm();
    if (formData) {
      this.setState({ form: formData });
    }
  }

  handleChange = (e, context) => {
    this.setState({ [context]: e.target.value });
  };

  handleSubmit = (e) => {
    e.preventDefault();
    console.log("clicked");
    const email = this.state.email;
    const password = this.state.password;

    Authenticate.authenticateUser(email, password).then((data) => {
      const result = data.status;
      const error = data.error;
      const role = data?.data?.userinfo.role;

      if (result === 200 && role === "B") {
        this.props.history.push({
          pathname: "/",
        });
      } else if (result === 200 && role === "S") {
        this.props.history.push({
          pathname: "/seller/products",
        });
      } else {
        this.setState({ message: error });
      }
    });
  };

  render() {
    let data = this.state.form.data;

    if (!data) {
      return <h4>Loading</h4>;
    }

    let fields = data?.formData?.sections[0].fields;

    return (
      <div className="form_login">
        <div className="form_container">
          <h1>Sign In</h1>
          <form>
            {fields &&
              fields.map((field, i) => {
                const inpData = data?.fieldsData[field];
                if (!inpData) {
                  return false;
                }
                return (
                  <Input key={i} data={inpData} action={this.handleChange} />
                );
              })}

            <div className="form__button">
              <Button
                type={"Submit"}
                text={"SIGN IN"}
                className={"yellowButton"}
                Click={this.handleSubmit}
              />
            </div>
            <p className="login__error">{this.state.message}</p>
          </form>
          <div className="form_newUser">
            <p> New User??</p>
            <Link to="/register">
              <Button
                type={"Submit"}
                className={"normalButton"}
                text={"Create Account"}
              />
            </Link>
          </div>
        </div>
      </div>
    );
  }
}
