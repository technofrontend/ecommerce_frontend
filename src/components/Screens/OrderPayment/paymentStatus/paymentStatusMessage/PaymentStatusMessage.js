import React from "react";
import "./PaymentStatusMessage.css";

class PaymentStatusMessage extends React.Component {
  render() {
    return (
      <div className="PaymentStatusMessage">
        <h2>Payment Successful</h2>
        <h4>Thank you for shoppping.</h4>
      </div>
    );
  }
}

export default PaymentStatusMessage;
