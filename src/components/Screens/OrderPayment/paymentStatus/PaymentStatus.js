import React from "react";
import "./PaymentStatus.css";
import PaymentStatusMesssage from "./paymentStatusMessage/PaymentStatusMessage";
import ProductList from "../orderReview/orderReviewDetails/productList/ProductList";
import Button from "../components/Button/Button";
import { Link } from "react-router-dom";

class PaymentStatus extends React.Component {
  render() {
    const productsData = this.props.location.state["productData"];

    return (
      <div className="PaymentStatus">
        <PaymentStatusMesssage />
        <div className="PaymentStatus_ProductList">
          <ProductList productsData={productsData} />
        </div>
        <div className="PaymentStatus_Button">
          <Link to="/">
            <Button value="Go to Homepage" onclick={()=>{}}/>
          </Link>
        </div>
      </div>
    );
  }
}

export default PaymentStatus;
