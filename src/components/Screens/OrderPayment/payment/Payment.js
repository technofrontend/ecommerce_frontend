import React from 'react';
import './Payment.css';
import PageHeading from '../components/PageHeading/PageHeading';
import PaymentModes from './paymentModes/PaymentModes';
import Button from '../components/Button/Button';
import {Link} from 'react-router-dom';
import axios from 'axios';
import Cookies from 'universal-cookie';

class Payment extends React.Component {
    constructor(){
        super();
        this.state = {isDisabled:true, errorMessage:''};
        this.handleOrderButton = this.handleOrderButton.bind(this);
        this.createOrder = this.createOrder.bind(this);
    }
    handleOrderButton(disabled,message){
        this.setState({
            isDisabled: disabled,
            errorMessage: message
        });
    }

    render() {
        const {amount,cart} = this.props.location.state;
        const {allproducts} = cart;

        return (
            <div className='Payment'>
                <PageHeading  heading='Select a payment method' />
                <PaymentModes amount ={amount} handleOrderButton={this.handleOrderButton}/>
                <div className='ErrorMessage'>
                    {
                        this.state.isDisabled && <p>{this.state.errorMessage}</p>
                    }
                </div>
                <div className='Payment_Button'>
                    <Link to={{
                        pathname : '/paymentStatus',
                        state : {
                            'productData' : allproducts
                        }
                    }}>
                        <Button value='Place your order' onclick={this.createOrder} disabled={this.state.isDisabled}/>
                    </Link>
                </div>
            </div>
        )
    }

    createOrder(){
        const list = this.props.location.state['cart'].allproducts;
        let productList = list.map((product)=>{
            return {
                'pid': product.product.pid,
                'quantity': product.quantity
            }; 
        })

        const orderData = { 
            'list' : productList,
            'total_amount' : this.props.location.state['amount'],
            "address":{
                "address":"567,citi ",
                "city":"London",
                "country":"Usa",
                "pincode":"123488"
            },
            "phoneno":"1234"          
        };

        let cookies = new Cookies();
        const jwtToken = cookies.get("uuid");
        axios({
            method: "post",
            url: "https://protected-basin-07111.herokuapp.com/orders/create-order",
            headers: { 
                "Authorization": `Bearer ${jwtToken}` ,
                "Content-Type": "application/json" 
            },
            data: orderData
        }).then((res)=>{
            console.log(res.data);
        })
    }

}

export default Payment;
