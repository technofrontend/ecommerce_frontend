import React from 'react';
import axios from 'axios';
import './Wallet.css';
import Cookies from "universal-cookie";

class Wallet extends React.Component {
    constructor(){
        super();
        this.state = {balance:0};
    }

    componentDidMount(){
        let cookies = new Cookies();
        const jwtToken = cookies.get("uuid");
        axios({
            method: "get",
            url: "https://protected-basin-07111.herokuapp.com/users/get-amount",
            headers: { 
                "Authorization": `Bearer ${jwtToken}` ,
                "Content-Type": "application/json" 
            }
        }).then((res)=>{
            this.setState({balance:res.data.amount});
        })
    }


    render() {
        const styleClassName = this.props.isComponentDetailsVisible? 'SelectedStyle' : 'Wallet';

        return (
            <div className={styleClassName} >
                <input type='radio' id='wallet' name='mode' 
                    onChange={ (e)=>{
                        this.props.handleChange(e);
                        this.check();
                    }} 
                />
                <label htmlFor='wallet'>Wallet</label>
                {
                    this.props.isComponentDetailsVisible && <p>Your current wallet balance : Rs.{this.state.balance}</p> 
                }
            </div>
        )
    }

    check(){
        if(this.state.balance < this.props.billAmount)
            this.props.handleOrderButton(true,'* Insufficient balance in wallet');
        }
}

export default Wallet;