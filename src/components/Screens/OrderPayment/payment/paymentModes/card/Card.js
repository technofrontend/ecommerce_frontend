import React from "react";
import "./Card.css";
import CardDetails from "./cardDetails/CardDetails";

class Card extends React.Component {
  render() {
    const styleClassName = this.props.isComponentDetailsVisible ? "SelectedStyle" : "Card";

    return (
      <div className={styleClassName}>
        <input type="radio" id="card" name="mode" onChange={(e) => this.props.handleChange(e)} />
        <label htmlFor="card">Debit/Credit Card</label>

        <div className="Card_Logo">
          <img
            src="https://hitechselfstorageunits.com/wp-content/uploads/2020/10/credit-cards-1.jpg"
            alt="notfound"
          />
        </div>

        {this.props.isComponentDetailsVisible && (
          <CardDetails handleOrderButton={this.props.handleOrderButton} />
        )}
      </div>
    );
  }
}

export default Card;
