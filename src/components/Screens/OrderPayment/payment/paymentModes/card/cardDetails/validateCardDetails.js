export const validateName = (nameOnCard)=> {
    const regex = /^[a-zA-Z]+(([ a-zA-Z][a-zA-Z ])?)*$/;
    return regex.test(nameOnCard);
}

export const validateNumber = (cardNumber) => {
    //Visa, MasterCard
    const regex = [
        /^4[0-9]{12}(?:[0-9]{3})?$/,
        /^(?:4[0-9]{12}(?:[0-9]{3})?|5[1-5][0-9]{14})$/
    ];
    let isValid = false;
    for(let i=0 ; i<regex.length; i++){
        if(regex[i].test(cardNumber)){
            isValid=true;
            break;
        }
    }
    return isValid;
}

export const validateCVV = (cvv) => {
    if(cvv.length !== 3)
        return false;

    const regex = /^[0-9]/;
    return regex.test(cvv);
}