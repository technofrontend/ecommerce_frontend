import React from "react";
import "./CardDetails.css";
import {
  validateName,
  validateNumber,
  validateCVV,
} from "./validateCardDetails";

class CardDetails extends React.Component {
  constructor() {
    super();
    this.state = {
      nameOnCard: "",
      cardNumber: 0,
      cvv: "",
      invalidNameError: false,
      invalidCardNumberError: false,
      invalidCvvError: false,
    };
    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  componentDidMount() {
    this.props.handleOrderButton(true, "");
  }

  handleChange(event, context) {
    this.setState({ [context]: event.target.value });
  }

  handleSubmit(e) {
    e.preventDefault();
    if (
      !this.state.invalidNameError &&
      !this.state.invalidCardNumberError &&
      !this.state.invalidCvvError
    )
      this.props.handleOrderButton(false, "");
  }

  render() {
    return (
      <div className="CardDetails">
        <form onSubmit={this.handleSubmit}>
          <div className="CardName">
            <label htmlFor="nameOnCard">Name on card</label>
            <input
              type="text"
              id="nameOnCard"
              value={this.state.name}
              onChange={(e) => this.handleChange(e, "nameOnCard")}
              onBlur={() => {
                if (!validateName(this.state.nameOnCard)) {
                  this.setState({ invalidNameError: true });
                  this.props.handleOrderButton(true, "* Enter valid card holder name");
                } else {
                  this.setState({ invalidNameError: false });
                  this.props.handleOrderButton(true, "");
                }
              }}
              required
            />
          </div>

          <div className="CardNumber">
            <label htmlFor="cardNumber">Card Number</label>
            <input
              type="text"
              id="cardNumber"
              value={this.state.number}
              onChange={(e) => this.handleChange(e, "cardNumber")}
              onBlur={() => {
                if (!validateNumber(this.state.cardNumber)) {
                  this.setState({ invalidCardNumberError: true });
                  this.props.handleOrderButton(true, "* Enter valid card number");
                } else {
                  this.setState({ invalidCardNumberError: false });
                  this.props.handleOrderButton(true, "");
                }
              }}
              required
            />
          </div>

          <div className="Card_Expiry">
            <label htmlFor="month">Expiry Date</label>
            <div>
              <select id="month">
                <option>01</option>
                <option>02</option>
                <option>03</option>
                <option>04</option>
                <option>05</option>
                <option>06</option>
                <option>07</option>
                <option>08</option>
                <option>09</option>
                <option>10</option>
                <option>11</option>
                <option>12</option>
              </select>

              <select id="year">
                <option>2021</option>
                <option>2022</option>
                <option>2023</option>
                <option>2024</option>
                <option>2025</option>
                <option>2026</option>
                <option>2027</option>
              </select>
            </div>
          </div>

          <div className="Card_CVV">
            <label htmlFor="cvv">CVV</label>
            <input
              type="password"
              id="cvv"
              value={this.state.cvv}
              onChange={(e) => this.handleChange(e, "cvv")}
              onBlur={() => {
                if (!validateCVV(this.state.cvv)) {
                  this.setState({ invalidCvvError: true });
                  this.props.handleOrderButton(true, "* Enter valid CVV");
                } else {
                  this.setState({ invalidCvvError: false });
                  this.props.handleOrderButton(true, "");
                }
              }}
              required
            />
          </div>
          <input type="submit" value="OK" />
        </form>
      </div>
    );
  }
}

export default CardDetails;