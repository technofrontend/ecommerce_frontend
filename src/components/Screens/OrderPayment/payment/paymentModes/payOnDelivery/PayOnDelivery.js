import React from 'react';
import './PayOnDelivery.css';

class PayOnDelivery extends React.Component {
    render() {
        const styleClassName = this.props.isComponentDetailsVisible? 'SelectedStyle' : 'PayOnDelivery';

        return (
            <div className={styleClassName}>
                <input type='radio' id='payOnDelivery' name='mode' onChange={(e)=>this.props.handleChange(e)} />
                <label htmlFor='payOnDelivery'>Pay on Delivery</label>
                {
                    this.props.isComponentDetailsVisible && <p>Total amount : Rs.{this.props.billAmount}</p> 
                }
            </div>
        )
    }
}

export default PayOnDelivery;