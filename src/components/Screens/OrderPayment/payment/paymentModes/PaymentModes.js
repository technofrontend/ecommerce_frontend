import React from 'react';
import './PaymentModes.css';
import Card from './card/Card';
import PayOnDelivery from './payOnDelivery/PayOnDelivery';
import Wallet from './wallet/Wallet';

class PaymentModes extends React.Component {
    constructor(){
        super();
        this.state = {paymentModes:[
            {
                componentName : 'card',
                isVisible : false
            },
            {   
                componentName : 'payOnDelivery',
                isVisible : false

            },
            {
                componentName : 'wallet',
                isVisible : false
            }
        ]}
        this.handleChange = this.handleChange.bind(this);
    }

    handleChange(event){
        const checkedComponentName = event.target.id;
        const array = [...this.state.paymentModes];

        const checkedComponent = array.find((component) => component.componentName === checkedComponentName)

        array.forEach((component)=>component.isVisible = false)
        checkedComponent.isVisible = true;
        this.props.handleOrderButton(false,'');

        this.setState({
            paymentModes : array
        })
    }

    isComponentVisible(componentName){
        const component = this.state.paymentModes.find((component) => component.componentName === componentName)
        return component.isVisible;
    }

    render() {
        return (
            <div className='PaymentModes'>
                <Card handleChange={this.handleChange} 
                        isComponentDetailsVisible={this.isComponentVisible('card')} 
                        handleOrderButton={this.props.handleOrderButton} 
                />
                <PayOnDelivery handleChange={this.handleChange} 
                        isComponentDetailsVisible={this.isComponentVisible('payOnDelivery')}  
                        billAmount={this.props.amount} 
                />
                <Wallet handleChange={this.handleChange} 
                        isComponentDetailsVisible={this.isComponentVisible('wallet')} 
                        billAmount={this.props.amount} 
                        handleOrderButton={this.props.handleOrderButton} 
                />
            </div>
        )
    }

}

export default PaymentModes;