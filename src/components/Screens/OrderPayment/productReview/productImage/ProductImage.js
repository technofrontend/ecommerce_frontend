import React from 'react';
import './ProductImage.css';

class ProductImage extends React.Component {
    render() {
        const {info,name} = this.props.product;
        
        return (
            <div className='ProductImage'>
                <img src={info.imageurl} alt='no-img'/>
                <div className='ProductTitle'>
                    <p>{name}</p>
                </div>
            </div>
        )
    }
}
export default ProductImage;