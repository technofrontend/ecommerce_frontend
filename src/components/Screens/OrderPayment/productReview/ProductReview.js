import React from 'react';
import './ProductReview.css';
import axios from 'axios';
import ProductImage from './productImage/ProductImage';
import Review from './review/Review';
import Cookies from 'universal-cookie';

class ProductReview extends React.Component {
    constructor(){
        super();
        this.state= {ratings:0, comments:'', successMessage:false};
        this.handleRatingsChange = this.handleRatingsChange.bind(this);
        this.handleComments = this.handleComments.bind(this);
        this.submitReview = this.submitReview.bind(this);
    }

    handleRatingsChange(ratings){
        this.setState({ratings:ratings});
    }

    handleComments(comments){
        this.setState({comments:comments});
    }

    submitReview(){
        const {comments,ratings} = this.state;
        const {product} = this.props.location.state;
        let cookies = new Cookies();
        const jwtToken = cookies.get("uuid");
        axios({
            "method" : "post",
            "url" : "https://protected-basin-07111.herokuapp.com/review/save",
            "headers" : {
                "Authorization": `Bearer ${jwtToken}`,
                "Content-Type": "application/json",
            },
            "data" : {
                    "pid" : product.pid,
                    "comment" : comments,
                    "rating" : ratings
            }
        }).then((res)=>{
            this.setState({successMessage:true});
        })
        this.handleRatingsChange(0);
        this.handleComments('');
    }

    render() {
        const {product} = this.props.location.state;
        const {comments,ratings,successMessage} = this.state;
        return (
            <div className='ProductReview'>
                <ProductImage product={product}/>
                <Review comments={comments} ratings={ratings} successMessage={successMessage} handleComments={this.handleComments} handleRatingsChange={this.handleRatingsChange} submitReview={this.submitReview} />
            </div>
        )
    }
}

export default ProductReview;