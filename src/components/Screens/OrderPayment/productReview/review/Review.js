import React from 'react';
import './Review.css';
import PageHeading from '../../components/PageHeading/PageHeading';
import Comments from './comments/Comments';
import Ratings from './ratings/Ratings';
import Button from '../../components/Button/Button';

class Review extends React.Component {
    render() {
        return (
            <div className='Review'>
                <PageHeading heading='Write product review' />
                <Comments handleComments={this.props.handleComments}/>
                <Ratings handleRatingsChange={this.props.handleRatingsChange} />
                <Button value='Submit Review' onclick={this.props.submitReview} disabled={this.check()}/>
                <div className='SuccessMessage'>
                {
                    this.props.successMessage? <p>Thank you for your response!</p> : ''
                }
                </div>
            </div>
        )
    }

    check(){
        if(!this.props.ratings || this.props.comments==='')
            return true;
        else
            return false
    }

}

export default Review;