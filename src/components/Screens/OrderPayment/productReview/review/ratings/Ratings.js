import React from "react";
import "./Ratings.css";

class Ratings extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      stars: [],
      rating: 0,
      hovered: 0,
      selectedIcon: "★",
      deselectedIcon: "☆",
    };

    for (var i = 0; i < 5; i++) {
      this.state.stars.push(i + 1);
    }
  }

  changeRating(newRating) {
    this.setState({ rating: newRating });
    this.props.handleRatingsChange(newRating);
  }

  hoverRating(rating) {
    this.setState({ hovered: rating });
  }

  render() {
    const { stars, rating, hovered, deselectedIcon, selectedIcon } = this.state;

    return (
      <div className="Ratings">
        {
        stars.map((star) => {
            return (
                <span key={star}
                style={{ cursor: "pointer" }}
                onClick={() => this.changeRating(star)}
                onMouseEnter={() => this.hoverRating(star)}
                onMouseLeave={() => this.hoverRating(0)}
                >
                {rating < star
                    ? hovered < star
                    ? deselectedIcon
                    : selectedIcon
                    : selectedIcon}
                </span>
            );
            })
        }
      </div>
    );
  }
}

export default Ratings;
