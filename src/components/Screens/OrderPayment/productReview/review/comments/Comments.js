import React from 'react';
import './Comments.css';

class Comments extends React.Component {
    render() {
        return (
            <div className='Comments'>
                <h3>Comments</h3>
                <textarea onBlur={(event)=> this.props.handleComments(event.target.value)}/>
            </div>
        )
    }
}

export default Comments;