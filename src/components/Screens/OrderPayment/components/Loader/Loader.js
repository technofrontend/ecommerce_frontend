import React from 'react';
import './Loader.css';

class Loader extends React.Component {
    render() {
        return (
            <div className='Loader'>
                <div className='LoaderText'>Loading</div>
                <div className='LoaderIcon'></div>
            </div>
        )
    }
}
export default Loader;