import React from 'react';
import './Button.css';

class Button extends React.Component {
    render() {
        return (
            <div>
                {
                    this.props.disabled 
                        ? <button className='Button' onClick={()=>this.props.onclick()} disabled>{this.props.value}</button> 
                        : <button className='Button' onClick={()=>this.props.onclick()}><span>{this.props.value}</span></button>
                       
                }   
            </div>
        )
    }
}

export default Button;