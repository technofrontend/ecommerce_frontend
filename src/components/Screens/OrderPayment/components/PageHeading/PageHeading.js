import React from 'react';
import './PageHeading.css';

class PageHeading extends React.Component {
    render() {
        return (
            <div className='PageHeading'>
                <p>{this.props.heading}</p>
                <hr/>
            </div>
        )
    }
}

export default PageHeading;