import React from 'react';
import './Product.css';
import Image from '../Image/Image';
import ProductDetails from './ProductDetails/ProductDetails';

class Product extends React.Component {
    render() {
        return (
            <div className="Product">
                <Image src={this.props.product?.product?.info?.imageurl} />
                <ProductDetails product={this.props.product}/>
            </div>
        )
    }
}

export default Product;