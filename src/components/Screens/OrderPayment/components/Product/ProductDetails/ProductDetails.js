import React from 'react';
import './ProductDetails.css';

class ProductDetails extends React.Component {
    render() {
        const {product} = this.props;
        return (
            <div className='ProductDetails'>
                <p className='ProductDetails_Name'>{product.product.name}</p>
                <p className='ProductDetails_Price'>&#8377; {product.product.listedprice}
                    <span><strike>MRP: &#8377; {product.product.actualprice}</strike> </span>
                </p>
                <p>Quantity : {product.quantity}</p>
            </div>
        )
    }
}

export default ProductDetails;