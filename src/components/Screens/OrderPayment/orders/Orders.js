import React from "react";
import "./Orders.css";
import PageHeading from "../components/PageHeading/PageHeading";
import PlacedOrders from "./placedOrders/PlacedOrders";
import CancelledOrders from "./cancelledOrders/CancelledOrders";

class Orders extends React.Component {
    constructor(){
      super();
      this.state = {displayPlacedOrders : true};
    }

    toggleDisplayPlacedOrders(isSelected){
      this.setState({displayPlacedOrders : isSelected});
    }

  render() {
    const orderTabClassName = this.state.displayPlacedOrders ? 'Selected' : 'Unselected';
    const cancelledOrderTabClassName = this.state.displayPlacedOrders ? 'Unselected' : 'Selected';
    return (
      <div className='Orders'>
        <PageHeading heading="Your Orders" />
        <div className='Orders_Tabs'>
          <button className={orderTabClassName} onClick={()=> this.toggleDisplayPlacedOrders(true) }>Orders</button>
          <button className={cancelledOrderTabClassName} onClick={()=> this.toggleDisplayPlacedOrders(false) } >Cancelled Orders</button>
        </div>
        {this.state.displayPlacedOrders ? <PlacedOrders /> : <CancelledOrders /> }
      </div>
    );
  }
}

export default Orders;