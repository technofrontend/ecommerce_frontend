import React from 'react';
import './PlacedOrders.css';
import axios from "axios";
import OrderFilter from "../OrderFilter/OrderFilter";
import OrderDetails from "./orderDetails/OrderDetails";
import Loader from "../../components/Loader/Loader";
import Cookies from "universal-cookie";

class PlacedOrders extends React.Component {
    constructor(props) {
        super(props);
        this.state = { orders: [], year: "2021" };
        this.handleFilterChange = this.handleFilterChange.bind(this);
      }
    
      componentDidMount() {
        let cookies = new Cookies();
        const jwtToken = cookies.get("uuid");
        axios({
          method: "get",
          url: "https://protected-basin-07111.herokuapp.com/orders/list",
          headers: {
            Authorization: `Bearer ${jwtToken}`,
            "Content-Type": "application/json",
          },
        }).then((res) => {
          this.setState({ orders: res.data });
        });
      }
      handleFilterChange(event) {
        this.setState({ year: event.target.value });
      }
    
      render() {
        return (
          <div>
            <OrderFilter handleFilterChange={this.handleFilterChange} />
            {this.renderFilteredOrders()}
          </div>
        );
      }
    
      renderFilteredOrders() {
        let isOrderFound = false;
        const orders = this.state.orders;
        const START_YEAR_INDEX = 0;
        const END_YEAR_INDEX = 4;
    
        if(this.state.orders.length === 0)
          return <Loader />
        else{
          return (
            <div className="PlacedOrders">
              {orders?.map((order) => {
                const {createdOn:date} = order;
                  
                if (date.substring(START_YEAR_INDEX, END_YEAR_INDEX) === this.state.year) {
                  isOrderFound = true;
                  return (
                    <div key={order.orderid} className="PlacedOrders-order">
                      <OrderDetails order={order} />
                    </div>
                  );
                } else return null;
              })}
              {!isOrderFound && (
                <div className='PlacedOrderMessage'>You have not placed any orders in {this.state.year}. </div>
              )}
            </div>
          );
        }
        
      }
}

export default PlacedOrders;
