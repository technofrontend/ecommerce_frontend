import React from "react";
import Button from "../../../../components/Button/Button";
import "./OrderButtons.css";
import {Link} from 'react-router-dom';
import axios from 'axios';
import Cookies from 'universal-cookie';

class OrderButtons extends React.Component {
  returnProduct(){
    const {order,product} = this.props;
        let cookies = new Cookies();
        const jwtToken = cookies.get("uuid");
        axios({
          method: "post",
          url: "https://protected-basin-07111.herokuapp.com/return-product",
          headers: {
            Authorization: `Bearer ${jwtToken}`,
            "Content-Type": "application/json",
          },
          data:{
            "order-id" : order.orderId,
            "pair"  :{
              "pid" : product.pid,
              "quantity" :  product.quantity
            }
          }
        }).then((res) => {
          
        });
  }
  render() {
    const {product} = this.props.product;
    
    return (
      <div className="OrderButtons">
        <div className="OrderButtons_Button">
          <Button value="Return product" onclick={() => this.returnProduct()} />
        </div>
        <div className="OrderButtons_Button">
          <Link to={{
            pathname : '/productReview',
            state : {
                product : product
            }
            }}>
              <Button value="Write a product review" onclick={() => {}} />
          </Link>
        </div>
      </div>
    );
  }
}

export default OrderButtons;