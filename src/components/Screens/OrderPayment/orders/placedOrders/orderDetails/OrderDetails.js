import React from 'react';
import './OrderDetails.css';
import OrderHeader from './orderHeader/OrderHeader';
import OrderCard from './orderCard/OrderCard';

class OrderDetails extends React.Component {
    render() {
        return (
            <div className="OrderDetails">
                <OrderHeader order={this.props.order} />
                <OrderCard order={this.props.order} productList={this.props.order?.productsinfo} />
            </div>
        )
    }
}

export default OrderDetails;