import React from 'react';
import './OrderHeader.css';

class OrderHeader extends React.Component {
    render() {
        const {createdOn,orderamount,orderid} = this.props.order;
        const date = createdOn.split(' ');
        return (
            <div className="OrderHeader">
                <div className="left-div">
                    <div>
                        <p>ORDER PLACED</p>
                        <p>{date[0]}</p>
                    </div>
                    <div>
                        <p> TOTAL</p>
                        <p>Rs.{orderamount}</p>
                    </div>
                    <div>
                        <p>SHIP TO</p>
                        <p className="Ship-to">ABC
                            <span className='TooltipText'>hey! thats's your shipping address</span>
                        </p>
                    </div>
                </div>
                <div className="right-div">
                    <p>ORDER# &nbsp;{orderid}</p>
                </div>
            </div>
        )
    }
}

export default OrderHeader;