import React from 'react';
import './ProductCard.css';
import Product from '../../../../../components/Product/Product';
import OrderButtons from '../../orderButtons/OrderButtons';

class ProductCard extends React.Component {
    render() {
        return (
            <div className='ProductCard'>
                <Product product={this.props.product} />
                <OrderButtons order={this.props.order} product={this.props.product}/>
            </div>
        )
    }
}

export default ProductCard;