import React from 'react';
import './OrderCard.css';
import ProductCard from './productCard/ProductCard';

class OrderCard extends React.Component {
    render() {
       return (
            <div className="OrderCard">
                {this.props.productList?.map((product) => <ProductCard key={product.product.pid} product={product} order={this.props.order} />)}
            </div>
        )
    }
}

export default OrderCard;