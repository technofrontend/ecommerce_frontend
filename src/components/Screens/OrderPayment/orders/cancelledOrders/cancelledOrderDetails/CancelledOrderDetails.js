import React from 'react';
import './CancelledOrderDetails.css';
import CancelledOrderCard from './cancelledOrderCard/CancelledOrderCard';

export default class CancelledOrderDetails extends React.Component {
    render() {
        return (
            <div className='CancelledOrderDetails'>
                <CancelledOrderCard />
            </div>
        )
    }
}
