import React from 'react';
import './CancelledOrders.css';
import axios from "axios";
import CancelledOrderDetails from './cancelledOrderDetails/CancelledOrderDetails';
import Cookies from "universal-cookie";

class CancelledOrders extends React.Component {
    constructor(props) {
        super(props);
        this.state = { cancelledOrders: []};
    }

    componentDidMount() {
        let cookies = new Cookies();
        const jwtToken = cookies.get("uuid");
        axios({
          method: "get",
          url: "https://protected-basin-07111.herokuapp.com/orders/list",
          headers: {
            Authorization: `Bearer ${jwtToken}`,
            "Content-Type": "application/json",
          },
        }).then((res) => {
        //   this.setState({ orders: res.data });
        });
    }

    render() {
        const {cancelledOrders} = this.state;
        return (
            <div className='CancelledOrders'>
                <CancelledOrderDetails cancelledOrders={cancelledOrders}/>
            </div>
        )
    }
}

export default CancelledOrders;