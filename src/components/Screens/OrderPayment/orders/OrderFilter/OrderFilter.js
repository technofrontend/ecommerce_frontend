import React from 'react';
import './OrderFilter.css';

class OrderPageFilters extends React.Component {
    render() {
        return (
            <div className="OrderFilter">
                Orders placed in 
                <select onChange={(e)=> this.props.handleFilterChange(e)}>
                    <option value="2021">2021</option>
                    <option value="2020">2020</option>
                    <option value="2019">2019</option>
                    <option value="2018">2018</option>
                </select>
            </div>
        )
    }
}

export default OrderPageFilters;