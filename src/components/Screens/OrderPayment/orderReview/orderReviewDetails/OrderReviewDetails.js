import React from 'react';
import './OrderReviewDetails.css';
import ProductList from './productList/ProductList';
import BillDetails from  './billDetails/BillDetails';

class OrderReviewDetails extends React.Component {
    
    render() {
        return (
            <div className='OrderReviewDetails'>
                <BillDetails cartData={this.props.cartData} />
                <ProductList productsData={this.props.cartData['allproducts']} />
            </div>
        )
    }
}

export default OrderReviewDetails;