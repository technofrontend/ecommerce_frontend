import React from 'react';
import './CouponCard.css';
import axios from 'axios';
import Cookies from "universal-cookie";

class CouponCard extends React.Component {
    constructor(){
        super();
        this.state = {discount : 0,hidden:true};
    }
    applyCouponCode(){
        const {code} = this.props.coupon;
        const {billAmount} = this.props;
        let cookies = new Cookies();
        const jwtToken = cookies.get("uuid");
        axios({
            method: "post",
            url: "https://protected-basin-07111.herokuapp.com/couponcode/apply",
            headers: { 
                'Authorization': `Bearer ${jwtToken}` ,
                "Content-Type": "application/json" 
            },
            data:{
                "cart_amount" : billAmount,
                "code": code
            }
        }).then((res)=>{
            this.props.setCouponErrorMessage(`*Coupon applied ${code}`);
            this.setState({discount : res.data});
            this.props.setCouponDiscount(this.state.discount);
        }).catch((res)=>{
            this.props.setCouponErrorMessage(`*Coupon not applicable`);
        })
    }
    
    removeCouponCode(){
        this.props.setCouponErrorMessage('');
        this.props.setCouponDiscount(0);
    }

    render() {
        const {code,isApplied} = this.props.coupon;

        return (
            <div className='CouponCard'>
                <div className='CouponCode'>
                    <p>{code}</p>
                </div>
                <div className='ApplyCouponButton'>
                    <button onClick={() =>{
                        this.setState({hidden:false});
                        this.applyCouponCode();
                        this.props.applyCoupon(code)}} >Apply coupon</button>
                </div>
                <div className='RemoveCouponButton'>
                    {isApplied && <button hidden={this.state.hidden} onClick={()=>{
                        this.removeCouponCode();
                        this.setState({hidden:true});
                        }} >x</button>}
                </div>
            </div>
        )
    }
}

export default CouponCard;