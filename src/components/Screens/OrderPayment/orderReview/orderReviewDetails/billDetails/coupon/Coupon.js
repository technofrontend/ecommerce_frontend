import React from 'react';
import './Coupon.css';
import CouponList from './couponList/CouponList';

class Coupon extends React.Component {
    constructor(){
        super();
        this.state = {isCouponListVisible : false};
        this.showCouponList = this.showCouponList.bind(this);
    }

    showCouponList(){
        const toggle = !(this.state.isCouponListVisible);
        this.setState({isCouponListVisible:toggle});
        this.props.setCouponDiscount(0);
    }

    render() {
        return (
            <div className='Coupon'>
                    <button onClick={()=> this.showCouponList()}>Coupon Codes</button>
                    <div className='CouponDetails'>
                    {
                        this.state.isCouponListVisible && <CouponList setCouponDiscount={this.props.setCouponDiscount} billAmount={this.props.billAmount} />
                    }
                    </div>
            </div>
        )
    }
}

export default Coupon;