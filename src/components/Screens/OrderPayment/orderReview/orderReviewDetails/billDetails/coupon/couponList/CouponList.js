import React from 'react';
import './CouponList.css';
import CouponCard from './couponCard/CouponCard';
import axios from 'axios';
import Cookies from "universal-cookie";

class CouponList extends React.Component {
  constructor() {
    super();
    this.state = {
      couponList: [],
      couponErrorMessagge: "",
    };
    this.setCouponErrorMessage = this.setCouponErrorMessage.bind(this);
    this.applyCoupon = this.applyCoupon.bind(this);
  }

  componentDidMount() {
      let cookies = new Cookies();
      const jwtToken = cookies.get("uuid");
    axios({
      method: "get",
      url: "https://protected-basin-07111.herokuapp.com/couponcode/list",
      headers: {
        'Authorization': `Bearer ${jwtToken}` ,
        "Content-Type": "application/json",
      },
    }).then((res) => {
      if (res.data?.length !== 0) {
        const couponsList = res.data?.map((coupon) => {
          coupon.isApplied = false;
          return coupon;
        });
        this.setState({ couponList: couponsList });
      } else this.setCouponErrorMessage("*No coupons available");
    });
  }

  setCouponErrorMessage(errorMessage) {
    this.setState({ couponErrorMessage: errorMessage });
  }

  applyCoupon(code) {
    const couponsList = JSON.parse(JSON.stringify(this.state.couponList));
    couponsList.forEach((coupon) => (coupon.isApplied = coupon.code === code));
    this.setState({ couponList: couponsList });
  }

  render() {
    return (
      <div className="CouponList">
        <div>
          {this.state.couponList?.map((coupon) => (
            <CouponCard
              key={coupon.code}
              coupon={coupon}
              setCouponErrorMessage={this.setCouponErrorMessage}
              applyCoupon={this.applyCoupon}
              setCouponDiscount={this.props.setCouponDiscount}
              billAmount={this.props.billAmount}
            />
          ))}
        </div>
        <div className="CouponErrorMessage">
          <p>{this.state.couponErrorMessage}</p>
        </div>
      </div>
    );
  }
}
export default CouponList;
