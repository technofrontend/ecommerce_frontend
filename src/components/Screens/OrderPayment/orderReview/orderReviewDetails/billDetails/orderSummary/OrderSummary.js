import React from 'react';
import '../orderSummary/OrderSummary.css';

class OrderSummary extends React.Component {
    render() {
        const {billAmount,tax,deliveryCharges,discount} = this.props;
        return (
            <div className='OrderSummary'>
                <p className='OrderSummaryHeading'><b>Order Summary</b></p>
                <p><b>Subtotal : </b> &#8377; {billAmount}</p>
                <p><b>Delivery Charges: </b>  &#8377; {deliveryCharges}</p>
                <p><b>Tax:</b> &#8377; {tax}</p>
                <h3 className='OrderSummary_Total'>Total : &#8377; {billAmount + deliveryCharges + tax  - discount}</h3>
            </div>
        )
    }
}

export default OrderSummary;