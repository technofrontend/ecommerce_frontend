import React from "react";
import Button from "../../../components/Button/Button";
import "../billDetails/BillDetails.css";
import BuyerDetails from "./buyerDetails/BuyerDetails";
import OrderSummary from "./orderSummary/OrderSummary";
import Coupon from './coupon/Coupon';
import { Link } from "react-router-dom";

class BillDetails extends React.Component {
  constructor(){
    super();
    this.state = {discount:0};
    this.setCouponDiscount = this.setCouponDiscount.bind(this);
  }
  setCouponDiscount(discount){
    this.setState({discount:discount});
  }

  render() {
    const {cartData} = this.props;
    const { amount: billAmount } = cartData;
    const TAX = 50;
    const DELIVERY_CHARGES = 100;

    return (
        <div className="BillDetails">
          <BuyerDetails />
          <OrderSummary billAmount={billAmount} tax={TAX} deliveryCharges={DELIVERY_CHARGES} discount={this.state.discount}/>
          
          <Link to={{
              pathname: "/payment",
              state: {
                amount : billAmount + TAX + DELIVERY_CHARGES - this.state.discount,
                cart: cartData,
              },
            }}
          >
            <div className="BillDetails_Button">
              <Button onclick={() => {}} value="Proceed to pay" />
            </div>
          </Link>
        <Coupon setCouponDiscount={this.setCouponDiscount} billAmount={billAmount} />
       </div>
    );
  }
}

export default BillDetails;