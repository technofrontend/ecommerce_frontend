import React from "react";
import "../productList/ProductList.css";
import Product from "../../../components/Product/Product";

class ProductList extends React.Component {
  render() {
    return (
      <div className="ProductList">
        {this.props.productsData?.map((product) => (
          <Product key={product.product.pid} product={product} />
        ))}
      </div>
    )
  }
}

export default ProductList;
