import React from 'react';
import axios from 'axios';
import './OrderReview.css';
import PageHeading from '../components/PageHeading/PageHeading';
import OrderReviewDetails from './orderReviewDetails/OrderReviewDetails';
import Loader from '../components/Loader/Loader';
import Cookies from "universal-cookie";

class OrderReview extends React.Component {
    constructor(props){
        super(props);
        this.state = {cart:{}};
    }

    componentDidMount(){
        let cookies = new Cookies();
        const jwtToken = cookies.get("uuid");
        axios({
            method: "get",
            url: "https://protected-basin-07111.herokuapp.com/user/cart/get-cart",
            headers: { 
                'Authorization': `Bearer ${jwtToken}` ,
                "Content-Type": "application/json" 
            }
        }).then((res)=>{
            this.setState({cart:res.data});
        })
    }

    render() {
        if(Object.keys(this.state.cart).length === 0){
            return <Loader/>
        }
        else{
            return (
                <div className="OrderReview">
                    <PageHeading heading='Review Your Order' />
                    <OrderReviewDetails cartData={this.state.cart} />
                </div>
            )
        }
    }
}

export default OrderReview;
