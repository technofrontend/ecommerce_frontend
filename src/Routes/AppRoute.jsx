import React, { Component } from "react";
import { BrowserRouter as Router, Switch } from "react-router-dom";
import PublicRoute from "./PublicRoute";
import SellerRoute from "./SellerRoute";
import BuyerRoute from "./BuyerRoute";

import "../App.css";
import HomePage from "../components/Screens/HomePage/HomePage";
import Login from "../components/Screens/LoginPage/Login";
import Register from "../components/Screens/RegisterPage/Register";
import Navbar from "../components/Shared/Navbar/Navbar";
import Footer from "../components/Shared/Footer/Footer";
import ProductList from "../components/Screens/ProductListPage/ProductList";
import ProductDetail from "../components/Screens/PDP/ProductDetail";
import Products from "../components/Screens/seller/products/Products";
import AddProduct from "../components/Screens/seller/add_product/AddProduct";
import EditProduct from "../components/Screens/seller/edit_product/EditProduct";
import BulkUpload from "../components/Screens/seller/bulk_update/BulkUpload";
import OrderReview from "../components/Screens/OrderPayment/orderReview/OrderReview";
import Orders from "../components/Screens/OrderPayment/orders/Orders";
import Payment from "../components/Screens/OrderPayment/payment/Payment";
import PaymentStatus from "../components/Screens/OrderPayment/paymentStatus/PaymentStatus";
import ProductReview from "../components/Screens/OrderPayment/productReview/ProductReview";
import WishlistMain from "../components/Screens/Wishlist/WishlistMain"
import CartLayout from "../components/Screens/Cart/CartLayout"


export default class AppRoute extends Component {
  constructor(props) {
    super(props);
    this.state = {
      user: {},
      isLoggedIn: false,
    };
  }

  static getDerivedStateFromProps(state) {
    const userData = localStorage.getItem("user");
    const user = JSON.parse(userData);

    if (user) {
      if (state.user !== userData) {
        return {
          user: user,
          isLoggedIn: true,
        };
      }
    }

    return null;
  }
  render() {
    let { isLoggedIn } = this.state;
    console.log(this.props);
    return (
      <div className="main_container">
        <Router>
          <div className="main_body">
            <Navbar user={isLoggedIn} />
            <Switch>
              <PublicRoute path="/login" component={Login} user={isLoggedIn} />
              <PublicRoute
                path="/register"
                component={Register}
                user={isLoggedIn}
              />
              <PublicRoute exact path="/:id/pl" component={ProductList} />
              <PublicRoute  path="/z:pid" component={ProductDetail} />
              <PublicRoute exact path="/" component={HomePage} />

              <SellerRoute
                exact
                path="/seller/products"
                component={Products}
                user={isLoggedIn}
              />
              <SellerRoute
                user={isLoggedIn}
                exact
                path="/seller/addproduct"
                component={AddProduct}
              />
              <SellerRoute
                user={isLoggedIn}
                exact
                path="/seller/editproduct"
                component={EditProduct}
              />
              <SellerRoute
                user={isLoggedIn}
                exact
                path="/seller/bulkupload"
                component={BulkUpload}
              />

              <BuyerRoute exact path="/orderReview" component={OrderReview} user={isLoggedIn} />
              <BuyerRoute exact path="/orders" component={Orders} user={isLoggedIn} />
              <BuyerRoute exact path="/payment" component={Payment} user={isLoggedIn} />
              <BuyerRoute exact path="/paymentStatus" component={PaymentStatus} user={isLoggedIn} />
              <BuyerRoute exact path="/productReview" component={ProductReview} user={isLoggedIn} />

              <BuyerRoute
                user={isLoggedIn}
                exact
                path="/myCart"
                component={CartLayout}
              />
              <BuyerRoute
                user={isLoggedIn}
                exact
                path="/Wishlist"
                component={WishlistMain}
              />
              {/* <Route component={NotFoundPage} path="*" /> */}
            </Switch>
          </div>
          <Footer />
        </Router>
      </div>
    );
  }
}
