import React, { Component } from "react";
import { Route,Redirect } from "react-router-dom";
export default class BuyerRoute extends Component {
  render() {
    const userData = localStorage.getItem("user");
    const userInfo = JSON.parse(userData);

    if (!userInfo) {
      return <Redirect to="/" />;
    }
    
    return <Route render={(props) => <this.props.component {...props} />} />;
  }
}
