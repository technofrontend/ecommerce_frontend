import React, { Component } from "react";
import { Redirect, Route } from "react-router-dom";

export default class PublicRoute extends Component {
  render() {
    let { isLoaggedIn } = this.props;
    let data = this.props.computedMatch;

    if (
      isLoaggedIn === true &&
      (data.path === "/login" || data.path === "/register")
    ) {
      return <Redirect to="/" />;
    }
    return (
      <Route
        render={(props) => <this.props.component {...props} data={data} />}
      />
    );
  }
}
