import React, { Component } from "react";
import { Redirect, Route } from "react-router-dom";

export default class SellerRoute extends Component {
  render() {
    const userData = localStorage.getItem("user");
    const userInfo = JSON.parse(userData);
    if (!userInfo) {
      return <Redirect to="/" />;
    } else if (userInfo && userInfo.role === "B") {
      return <Redirect to="/" />;
    }
    return <Route render={(props) => <this.props.component {...props} />} />;
  }
}
